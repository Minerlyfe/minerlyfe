Schmeckles Core 0.1.1
===============================



https://www..org


What are Schmeckles?
----------------

Schmeckles is the first interdementional digital currency supported by genius scientists 
throughout the multiverse.

The Shmeckle is now currency of choice allowing anyone, anywhere in the multiverse to make 
instant and anonymous payments with peer-to-peer technology.

The Schmeckle is backed by proof of work under the the X11 algorithm and the the Council of Ricks,
a body of councilors who serve to secure the network and to participate in the decentralized
governance system to allocate funds from the treasury to promote the interests of Ricks.

For more information, as well as an immediately useable, binary version of


Copyright
-------

This currency is supported by fans of the Rick and Morty television show. The developers make no
claim over the copyright over the characters or any of the intellectual property of the show. 

Schmeckles is a parody. It is a joke.

Schmeckles should not be taken seriously. 

Don't be rickdiculous.

License
-------

Schmeckles Core is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see https://opensource.org/licenses/MIT.

Development Process
-------------------

The `master` branch is meant to be stable. Development is normally done in separate branches.
[Tags](https://github.com/schmeckles/schmeckles/tags) are created to indicate new official,
stable release versions of Schmeckles Core.

The contribution workflow is described in [CONTRIBUTING.md](CONTRIBUTING.md).

Testing
-------

Testing and code review is the bottleneck for development; we get more pull
requests than we can review and test on short notice. Please be patient and help out by testing
other people's pull requests, and remember this is a security-critical project where any mistake might cost people
lots of money.

### Automated Testing

Developers are strongly encouraged to write [unit tests](/doc/unit-tests.md) for new code, and to
submit new unit tests for old code. Unit tests can be compiled and run
(assuming they weren't disabled in configure) with: `make check`

There are also [regression and integration tests](/qa) of the RPC interface, written
in Python, that are run automatically on the build server.
These tests can be run (if the [test dependencies](/qa) are installed) with: `qa/pull-tester/rpc-tests.py`

The Travis CI system makes sure that every pull request is built for Windows
and Linux, OS X, and that unit and sanity tests are automatically run.

### Manual Quality Assurance (QA) Testing

Changes should be tested by somebody other than the developer who wrote the
code. This is especially important for large or high-risk changes. It is useful
to add a test plan to the pull request description if testing the changes is
not straightforward.


