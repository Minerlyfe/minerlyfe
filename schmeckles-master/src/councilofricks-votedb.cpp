// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "councilofricks-votedb.h"

CCouncilofRicksObjectVoteFile::CCouncilofRicksObjectVoteFile()
    : nMemoryVotes(0),
      listVotes(),
      mapVoteIndex()
{}

CCouncilofRicksObjectVoteFile::CCouncilofRicksObjectVoteFile(const CCouncilofRicksObjectVoteFile& other)
    : nMemoryVotes(other.nMemoryVotes),
      listVotes(other.listVotes),
      mapVoteIndex()
{
    RebuildIndex();
}

void CCouncilofRicksObjectVoteFile::AddVote(const CCouncilofRicksVote& vote)
{
    listVotes.push_front(vote);
    mapVoteIndex[vote.GetHash()] = listVotes.begin();
    ++nMemoryVotes;
}

bool CCouncilofRicksObjectVoteFile::HasVote(const uint256& nHash) const
{
    vote_m_cit it = mapVoteIndex.find(nHash);
    if(it == mapVoteIndex.end()) {
        return false;
    }
    return true;
}

bool CCouncilofRicksObjectVoteFile::GetVote(const uint256& nHash, CCouncilofRicksVote& vote) const
{
    vote_m_cit it = mapVoteIndex.find(nHash);
    if(it == mapVoteIndex.end()) {
        return false;
    }
    vote = *(it->second);
    return true;
}

std::vector<CCouncilofRicksVote> CCouncilofRicksObjectVoteFile::GetVotes() const
{
    std::vector<CCouncilofRicksVote> vecResult;
    for(vote_l_cit it = listVotes.begin(); it != listVotes.end(); ++it) {
        vecResult.push_back(*it);
    }
    return vecResult;
}

void CCouncilofRicksObjectVoteFile::RemoveVotesFromCouncilor(const COutPoint& outpointCouncilor)
{
    vote_l_it it = listVotes.begin();
    while(it != listVotes.end()) {
        if(it->GetCouncilorOutpoint() == outpointCouncilor) {
            --nMemoryVotes;
            mapVoteIndex.erase(it->GetHash());
            listVotes.erase(it++);
        }
        else {
            ++it;
        }
    }
}

CCouncilofRicksObjectVoteFile& CCouncilofRicksObjectVoteFile::operator=(const CCouncilofRicksObjectVoteFile& other)
{
    nMemoryVotes = other.nMemoryVotes;
    listVotes = other.listVotes;
    RebuildIndex();
    return *this;
}

void CCouncilofRicksObjectVoteFile::RebuildIndex()
{
    mapVoteIndex.clear();
    nMemoryVotes = 0;
    vote_l_it it = listVotes.begin();
    while(it != listVotes.end()) {
        CCouncilofRicksVote& vote = *it;
        uint256 nHash = vote.GetHash();
        if(mapVoteIndex.find(nHash) == mapVoteIndex.end()) {
            mapVoteIndex[nHash] = it;
            ++nMemoryVotes;
            ++it;
        }
        else {
            listVotes.erase(it++);
        }
    }
}
