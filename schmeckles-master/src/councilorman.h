// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef COUNCILORMAN_H
#define COUNCILORMAN_H

#include "councilor.h"
#include "sync.h"

using namespace std;

class CCouncilorMan;
class CConnman;

extern CCouncilorMan mnodeman;

class CCouncilorMan
{
public:
    typedef std::pair<arith_uint256, CCouncilor*> score_pair_t;
    typedef std::vector<score_pair_t> score_pair_vec_t;
    typedef std::pair<int, CCouncilor> rank_pair_t;
    typedef std::vector<rank_pair_t> rank_pair_vec_t;

private:
    static const std::string SERIALIZATION_VERSION_STRING;

    static const int DSEG_UPDATE_SECONDS        = 3 * 60 * 60;

    static const int LAST_PAID_SCAN_BLOCKS      = 100;

    static const int MIN_POSE_PROTO_VERSION     = 70203;
    static const int MAX_POSE_CONNECTIONS       = 10;
    static const int MAX_POSE_RANK              = 10;
    static const int MAX_POSE_BLOCKS            = 10;

    static const int MNB_RECOVERY_QUORUM_TOTAL      = 10;
    static const int MNB_RECOVERY_QUORUM_REQUIRED   = 6;
    static const int MNB_RECOVERY_MAX_ASK_ENTRIES   = 10;
    static const int MNB_RECOVERY_WAIT_SECONDS      = 60;
    static const int MNB_RECOVERY_RETRY_SECONDS     = 3 * 60 * 60;


    // critical section to protect the inner data structures
    mutable CCriticalSection cs;

    // Keep track of current block height
    int nCachedBlockHeight;

    // map to hold all MNs
    std::map<COutPoint, CCouncilor> mapCouncilors;
    // who's asked for the Councilor list and the last time
    std::map<CNetAddr, int64_t> mAskedUsForCouncilorList;
    // who we asked for the Councilor list and the last time
    std::map<CNetAddr, int64_t> mWeAskedForCouncilorList;
    // which Councilors we've asked for
    std::map<COutPoint, std::map<CNetAddr, int64_t> > mWeAskedForCouncilorListEntry;
    // who we asked for the councilor verification
    std::map<CNetAddr, CCouncilorVerification> mWeAskedForVerification;

    // these maps are used for councilor recovery from COUNCILOR_NEW_START_REQUIRED state
    std::map<uint256, std::pair< int64_t, std::set<CNetAddr> > > mMnbRecoveryRequests;
    std::map<uint256, std::vector<CCouncilorBroadcast> > mMnbRecoveryGoodReplies;
    std::list< std::pair<CService, uint256> > listScheduledMnbRequestConnections;

    /// Set when councilors are added, cleared when CCouncilofRicksManager is notified
    bool fCouncilorsAdded;

    /// Set when councilors are removed, cleared when CCouncilofRicksManager is notified
    bool fCouncilorsRemoved;

    std::vector<uint256> vecDirtyCouncilofRicksObjectHashes;

    int64_t nLastWatchdogVoteTime;

    friend class CCouncilorSync;
    /// Find an entry
    CCouncilor* Find(const COutPoint& outpoint);

    bool GetCouncilorScores(const uint256& nBlockHash, score_pair_vec_t& vecCouncilorScoresRet, int nMinProtocol = 0);

public:
    // Keep track of all broadcasts I've seen
    std::map<uint256, std::pair<int64_t, CCouncilorBroadcast> > mapSeenCouncilorBroadcast;
    // Keep track of all pings I've seen
    std::map<uint256, CCouncilorPing> mapSeenCouncilorPing;
    // Keep track of all verifications I've seen
    std::map<uint256, CCouncilorVerification> mapSeenCouncilorVerification;
    // keep track of dsq count to prevent councilors from gaming darksend queue
    int64_t nDsqCount;


    ADD_SERIALIZE_METHODS;

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream& s, Operation ser_action, int nType, int nVersion) {
        LOCK(cs);
        std::string strVersion;
        if(ser_action.ForRead()) {
            READWRITE(strVersion);
        }
        else {
            strVersion = SERIALIZATION_VERSION_STRING; 
            READWRITE(strVersion);
        }

        READWRITE(mapCouncilors);
        READWRITE(mAskedUsForCouncilorList);
        READWRITE(mWeAskedForCouncilorList);
        READWRITE(mWeAskedForCouncilorListEntry);
        READWRITE(mMnbRecoveryRequests);
        READWRITE(mMnbRecoveryGoodReplies);
        READWRITE(nLastWatchdogVoteTime);
        READWRITE(nDsqCount);

        READWRITE(mapSeenCouncilorBroadcast);
        READWRITE(mapSeenCouncilorPing);
        if(ser_action.ForRead() && (strVersion != SERIALIZATION_VERSION_STRING)) {
            Clear();
        }
    }

    CCouncilorMan();

    /// Add an entry
    bool Add(CCouncilor &mn);

    /// Ask (source) node for mnb
    void AskForMN(CNode *pnode, const COutPoint& outpoint, CConnman& connman);
    void AskForMnb(CNode *pnode, const uint256 &hash);

    bool PoSeBan(const COutPoint &outpoint);
    bool AllowMixing(const COutPoint &outpoint);
    bool DisallowMixing(const COutPoint &outpoint);

    /// Check all Councilors
    void Check();

    /// Check all Councilors and remove inactive
    void CheckAndRemove(CConnman& connman);
    /// This is dummy overload to be used for dumping/loading mncache.dat
    void CheckAndRemove() {}

    /// Clear Councilor vector
    void Clear();

    /// Count Councilors filtered by nProtocolVersion.
    /// Councilor nProtocolVersion should match or be above the one specified in param here.
    int CountCouncilors(int nProtocolVersion = -1);
    /// Count enabled Councilors filtered by nProtocolVersion.
    /// Councilor nProtocolVersion should match or be above the one specified in param here.
    int CountEnabled(int nProtocolVersion = -1);

    /// Count Councilors by network type - NET_IPV4, NET_IPV6, NET_TOR
    // int CountByIP(int nNetworkType);

    void DsegUpdate(CNode* pnode, CConnman& connman);

    /// Versions of Find that are safe to use from outside the class
    bool Get(const COutPoint& outpoint, CCouncilor& councilorRet);
    bool Has(const COutPoint& outpoint);

    bool GetCouncilorInfo(const COutPoint& outpoint, councilor_info_t& mnInfoRet);
    bool GetCouncilorInfo(const CPubKey& pubKeyCouncilor, councilor_info_t& mnInfoRet);
    bool GetCouncilorInfo(const CScript& payee, councilor_info_t& mnInfoRet);

    /// Find an entry in the councilor list that is next to be paid
    bool GetNextCouncilorInQueueForPayment(int nBlockHeight, bool fFilterSigTime, int& nCountRet, councilor_info_t& mnInfoRet);
    /// Same as above but use current block height
    bool GetNextCouncilorInQueueForPayment(bool fFilterSigTime, int& nCountRet, councilor_info_t& mnInfoRet);

    /// Find a random entry
    councilor_info_t FindRandomNotInVec(const std::vector<COutPoint> &vecToExclude, int nProtocolVersion = -1);

    std::map<COutPoint, CCouncilor> GetFullCouncilorMap() { return mapCouncilors; }

    bool GetCouncilorRanks(rank_pair_vec_t& vecCouncilorRanksRet, int nBlockHeight = -1, int nMinProtocol = 0);
    bool GetCouncilorRank(const COutPoint &outpoint, int& nRankRet, int nBlockHeight = -1, int nMinProtocol = 0);
    bool GetCouncilorByRank(int nRank, councilor_info_t& mnInfoRet, int nBlockHeight = -1, int nMinProtocol = 0);

    void ProcessCouncilorConnections(CConnman& connman);
    std::pair<CService, std::set<uint256> > PopScheduledMnbRequestConnection();

    void ProcessMessage(CNode* pfrom, std::string& strCommand, CDataStream& vRecv, CConnman& connman);

    void DoFullVerificationStep(CConnman& connman);
    void CheckSameAddr();
    bool SendVerifyRequest(const CAddress& addr, const std::vector<CCouncilor*>& vSortedByAddr, CConnman& connman);
    void SendVerifyReply(CNode* pnode, CCouncilorVerification& mnv, CConnman& connman);
    void ProcessVerifyReply(CNode* pnode, CCouncilorVerification& mnv);
    void ProcessVerifyBroadcast(CNode* pnode, const CCouncilorVerification& mnv);

    /// Return the number of (unique) Councilors
    int size() { return mapCouncilors.size(); }

    std::string ToString() const;

    /// Update councilor list and maps using provided CCouncilorBroadcast
    void UpdateCouncilorList(CCouncilorBroadcast mnb, CConnman& connman);
    /// Perform complete check and only then update list and maps
    bool CheckMnbAndUpdateCouncilorList(CNode* pfrom, CCouncilorBroadcast mnb, int& nDos, CConnman& connman);
    bool IsMnbRecoveryRequested(const uint256& hash) { return mMnbRecoveryRequests.count(hash); }

    void UpdateLastPaid(const CBlockIndex* pindex);

    void AddDirtyCouncilofRicksObjectHash(const uint256& nHash)
    {
        LOCK(cs);
        vecDirtyCouncilofRicksObjectHashes.push_back(nHash);
    }

    std::vector<uint256> GetAndClearDirtyCouncilofRicksObjectHashes()
    {
        LOCK(cs);
        std::vector<uint256> vecTmp = vecDirtyCouncilofRicksObjectHashes;
        vecDirtyCouncilofRicksObjectHashes.clear();
        return vecTmp;;
    }

    bool IsWatchdogActive();
    void UpdateWatchdogVoteTime(const COutPoint& outpoint, uint64_t nVoteTime = 0);
    bool AddCouncilofRicksVote(const COutPoint& outpoint, uint256 nCouncilofRicksObjectHash);
    void RemoveCouncilofRicksObject(uint256 nCouncilofRicksObjectHash);

    void CheckCouncilor(const CPubKey& pubKeyCouncilor, bool fForce);

    bool IsCouncilorPingedWithin(const COutPoint& outpoint, int nSeconds, int64_t nTimeToCheckAt = -1);
    void SetCouncilorLastPing(const COutPoint& outpoint, const CCouncilorPing& mnp);

    void UpdatedBlockTip(const CBlockIndex *pindex);

    /**
     * Called to notify CCouncilofRicksManager that the councilor index has been updated.
     * Must be called while not holding the CCouncilorMan::cs mutex
     */
    void NotifyCouncilorUpdates(CConnman& connman);

};

#endif
