// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "activecouncilor.h"
#include "councilor.h"
#include "councilor-sync.h"
#include "councilorman.h"
#include "protocol.h"

extern CWallet* pwalletMain;

// Keep track of the active Councilor
CActiveCouncilor activeCouncilor;

void CActiveCouncilor::ManageState(CConnman& connman)
{
    LogPrint("councilor", "CActiveCouncilor::ManageState -- Start\n");
    if(!fCounciLor) {
        LogPrint("councilor", "CActiveCouncilor::ManageState -- Not a councilor, returning\n");
        return;
    }

    if(Params().NetworkIDString() != CBaseChainParams::REGTEST && !councilorSync.IsBlockchainSynced()) {
        nState = ACTIVE_COUNCILOR_SYNC_IN_PROCESS;
        LogPrintf("CActiveCouncilor::ManageState -- %s: %s\n", GetStateString(), GetStatus());
        return;
    }

    if(nState == ACTIVE_COUNCILOR_SYNC_IN_PROCESS) {
        nState = ACTIVE_COUNCILOR_INITIAL;
    }

    LogPrint("councilor", "CActiveCouncilor::ManageState -- status = %s, type = %s, pinger enabled = %d\n", GetStatus(), GetTypeString(), fPingerEnabled);

    if(eType == COUNCILOR_UNKNOWN) {
        ManageStateInitial(connman);
    }

    if(eType == COUNCILOR_REMOTE) {
        ManageStateRemote();
    } else if(eType == COUNCILOR_LOCAL) {
        // Try Remote Start first so the started local councilor can be restarted without recreate councilor broadcast.
        ManageStateRemote();
        if(nState != ACTIVE_COUNCILOR_STARTED)
            ManageStateLocal(connman);
    }

    SendCouncilorPing(connman);
}

std::string CActiveCouncilor::GetStateString() const
{
    switch (nState) {
        case ACTIVE_COUNCILOR_INITIAL:         return "INITIAL";
        case ACTIVE_COUNCILOR_SYNC_IN_PROCESS: return "SYNC_IN_PROCESS";
        case ACTIVE_COUNCILOR_INPUT_TOO_NEW:   return "INPUT_TOO_NEW";
        case ACTIVE_COUNCILOR_NOT_CAPABLE:     return "NOT_CAPABLE";
        case ACTIVE_COUNCILOR_STARTED:         return "STARTED";
        default:                                return "UNKNOWN";
    }
}

std::string CActiveCouncilor::GetStatus() const
{
    switch (nState) {
        case ACTIVE_COUNCILOR_INITIAL:         return "Node just started, not yet activated";
        case ACTIVE_COUNCILOR_SYNC_IN_PROCESS: return "Sync in progress. Must wait until sync is complete to start Councilor";
        case ACTIVE_COUNCILOR_INPUT_TOO_NEW:   return strprintf("Councilor input must have at least %d confirmations", Params().GetConsensus().nCouncilorMinimumConfirmations);
        case ACTIVE_COUNCILOR_NOT_CAPABLE:     return "Not capable councilor: " + strNotCapableReason;
        case ACTIVE_COUNCILOR_STARTED:         return "Councilor successfully started";
        default:                                return "Unknown";
    }
}

std::string CActiveCouncilor::GetTypeString() const
{
    std::string strType;
    switch(eType) {
    case COUNCILOR_UNKNOWN:
        strType = "UNKNOWN";
        break;
    case COUNCILOR_REMOTE:
        strType = "REMOTE";
        break;
    case COUNCILOR_LOCAL:
        strType = "LOCAL";
        break;
    default:
        strType = "UNKNOWN";
        break;
    }
    return strType;
}

bool CActiveCouncilor::SendCouncilorPing(CConnman& connman)
{
    if(!fPingerEnabled) {
        LogPrint("councilor", "CActiveCouncilor::SendCouncilorPing -- %s: councilor ping service is disabled, skipping...\n", GetStateString());
        return false;
    }

    if(!mnodeman.Has(outpoint)) {
        strNotCapableReason = "Councilor not in councilor list";
        nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
        LogPrintf("CActiveCouncilor::SendCouncilorPing -- %s: %s\n", GetStateString(), strNotCapableReason);
        return false;
    }

    CCouncilorPing mnp(outpoint);
    mnp.nSentinelVersion = nSentinelVersion;
    mnp.fSentinelIsCurrent =
            (abs(GetAdjustedTime() - nSentinelPingTime) < COUNCILOR_WATCHDOG_MAX_SECONDS);
    if(!mnp.Sign(keyCouncilor, pubKeyCouncilor)) {
        LogPrintf("CActiveCouncilor::SendCouncilorPing -- ERROR: Couldn't sign Councilor Ping\n");
        return false;
    }

    // Update lastPing for our councilor in Councilor list
    if(mnodeman.IsCouncilorPingedWithin(outpoint, COUNCILOR_MIN_MNP_SECONDS, mnp.sigTime)) {
        LogPrintf("CActiveCouncilor::SendCouncilorPing -- Too early to send Councilor Ping\n");
        return false;
    }

    mnodeman.SetCouncilorLastPing(outpoint, mnp);

    LogPrintf("CActiveCouncilor::SendCouncilorPing -- Relaying ping, collateral=%s\n", outpoint.ToStringShort());
    mnp.Relay(connman);

    return true;
}

bool CActiveCouncilor::UpdateSentinelPing(int version)
{
    nSentinelVersion = version;
    nSentinelPingTime = GetAdjustedTime();

    return true;
}

void CActiveCouncilor::ManageStateInitial(CConnman& connman)
{
    LogPrint("councilor", "CActiveCouncilor::ManageStateInitial -- status = %s, type = %s, pinger enabled = %d\n", GetStatus(), GetTypeString(), fPingerEnabled);

    // Check that our local network configuration is correct
    if (!fListen) {
        // listen option is probably overwritten by smth else, no good
        nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
        strNotCapableReason = "Councilor must accept connections from outside. Make sure listen configuration option is not overwritten by some another parameter.";
        LogPrintf("CActiveCouncilor::ManageStateInitial -- %s: %s\n", GetStateString(), strNotCapableReason);
        return;
    }

    // First try to find whatever local address is specified by externalip option
    bool fFoundLocal = GetLocal(service) && CCouncilor::IsValidNetAddr(service);
    if(!fFoundLocal) {
        bool empty = true;
        // If we have some peers, let's try to find our local address from one of them
        connman.ForEachNodeContinueIf(CConnman::AllNodes, [&fFoundLocal, &empty, this](CNode* pnode) {
            empty = false;
            if (pnode->addr.IsIPv4())
                fFoundLocal = GetLocal(service, &pnode->addr) && CCouncilor::IsValidNetAddr(service);
            return !fFoundLocal;
        });
        // nothing and no live connections, can't do anything for now
        if (empty) {
            nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
            strNotCapableReason = "Can't detect valid external address. Will retry when there are some connections available.";
            LogPrintf("CActiveCouncilor::ManageStateInitial -- %s: %s\n", GetStateString(), strNotCapableReason);
            return;
        }
    }

    if(!fFoundLocal) {
        nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
        strNotCapableReason = "Can't detect valid external address. Please consider using the externalip configuration option if problem persists. Make sure to use IPv4 address only.";
        LogPrintf("CActiveCouncilor::ManageStateInitial -- %s: %s\n", GetStateString(), strNotCapableReason);
        return;
    }

    int mainnetDefaultPort = Params(CBaseChainParams::MAIN).GetDefaultPort();
    if(Params().NetworkIDString() == CBaseChainParams::MAIN) {
        if(service.GetPort() != mainnetDefaultPort) {
            nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
            strNotCapableReason = strprintf("Invalid port: %u - only %d is supported on mainnet.", service.GetPort(), mainnetDefaultPort);
            LogPrintf("CActiveCouncilor::ManageStateInitial -- %s: %s\n", GetStateString(), strNotCapableReason);
            return;
        }
    } else if(service.GetPort() == mainnetDefaultPort) {
        nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
        strNotCapableReason = strprintf("Invalid port: %u - %d is only supported on mainnet.", service.GetPort(), mainnetDefaultPort);
        LogPrintf("CActiveCouncilor::ManageStateInitial -- %s: %s\n", GetStateString(), strNotCapableReason);
        return;
    }

    LogPrintf("CActiveCouncilor::ManageStateInitial -- Checking inbound connection to '%s'\n", service.ToString());

    if(!connman.ConnectNode(CAddress(service, NODE_NETWORK), NULL, true)) {
        nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
        strNotCapableReason = "Could not connect to " + service.ToString();
        LogPrintf("CActiveCouncilor::ManageStateInitial -- %s: %s\n", GetStateString(), strNotCapableReason);
        return;
    }

    // Default to REMOTE
    eType = COUNCILOR_REMOTE;

    // Check if wallet funds are available
    if(!pwalletMain) {
        LogPrintf("CActiveCouncilor::ManageStateInitial -- %s: Wallet not available\n", GetStateString());
        return;
    }

    if(pwalletMain->IsLocked()) {
        LogPrintf("CActiveCouncilor::ManageStateInitial -- %s: Wallet is locked\n", GetStateString());
        return;
    }

    if(pwalletMain->GetBalance() < 5000*COIN) {
        LogPrintf("CActiveCouncilor::ManageStateInitial -- %s: Wallet balance is < 5000 SCHMECKLES\n", GetStateString());
        return;
    }

    // Choose coins to use
    CPubKey pubKeyCollateral;
    CKey keyCollateral;

    // If collateral is found switch to LOCAL mode
    if(pwalletMain->GetCouncilorOutpointAndKeys(outpoint, pubKeyCollateral, keyCollateral)) {
        eType = COUNCILOR_LOCAL;
    }

    LogPrint("councilor", "CActiveCouncilor::ManageStateInitial -- End status = %s, type = %s, pinger enabled = %d\n", GetStatus(), GetTypeString(), fPingerEnabled);
}

void CActiveCouncilor::ManageStateRemote()
{
    LogPrint("councilor", "CActiveCouncilor::ManageStateRemote -- Start status = %s, type = %s, pinger enabled = %d, pubKeyCouncilor.GetID() = %s\n", 
             GetStatus(), GetTypeString(), fPingerEnabled, pubKeyCouncilor.GetID().ToString());

    mnodeman.CheckCouncilor(pubKeyCouncilor, true);
    councilor_info_t infoMn;
    if(mnodeman.GetCouncilorInfo(pubKeyCouncilor, infoMn)) {
        if(infoMn.nProtocolVersion != PROTOCOL_VERSION) {
            nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
            strNotCapableReason = "Invalid protocol version";
            LogPrintf("CActiveCouncilor::ManageStateRemote -- %s: %s\n", GetStateString(), strNotCapableReason);
            return;
        }
        if(service != infoMn.addr) {
            nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
            strNotCapableReason = "Broadcasted IP doesn't match our external address. Make sure you issued a new broadcast if IP of this councilor changed recently.";
            LogPrintf("CActiveCouncilor::ManageStateRemote -- %s: %s\n", GetStateString(), strNotCapableReason);
            return;
        }
        if(!CCouncilor::IsValidStateForAutoStart(infoMn.nActiveState)) {
            nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
            strNotCapableReason = strprintf("Councilor in %s state", CCouncilor::StateToString(infoMn.nActiveState));
            LogPrintf("CActiveCouncilor::ManageStateRemote -- %s: %s\n", GetStateString(), strNotCapableReason);
            return;
        }
        if(nState != ACTIVE_COUNCILOR_STARTED) {
            LogPrintf("CActiveCouncilor::ManageStateRemote -- STARTED!\n");
            outpoint = infoMn.vin.prevout;
            service = infoMn.addr;
            fPingerEnabled = true;
            nState = ACTIVE_COUNCILOR_STARTED;
        }
    }
    else {
        nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
        strNotCapableReason = "Councilor not in councilor list";
        LogPrintf("CActiveCouncilor::ManageStateRemote -- %s: %s\n", GetStateString(), strNotCapableReason);
    }
}

void CActiveCouncilor::ManageStateLocal(CConnman& connman)
{
    LogPrint("councilor", "CActiveCouncilor::ManageStateLocal -- status = %s, type = %s, pinger enabled = %d\n", GetStatus(), GetTypeString(), fPingerEnabled);
    if(nState == ACTIVE_COUNCILOR_STARTED) {
        return;
    }

    // Choose coins to use
    CPubKey pubKeyCollateral;
    CKey keyCollateral;

    if(pwalletMain->GetCouncilorOutpointAndKeys(outpoint, pubKeyCollateral, keyCollateral)) {
        int nPrevoutAge = GetUTXOConfirmations(outpoint);
        if(nPrevoutAge < Params().GetConsensus().nCouncilorMinimumConfirmations){
            nState = ACTIVE_COUNCILOR_INPUT_TOO_NEW;
            strNotCapableReason = strprintf(_("%s - %d confirmations"), GetStatus(), nPrevoutAge);
            LogPrintf("CActiveCouncilor::ManageStateLocal -- %s: %s\n", GetStateString(), strNotCapableReason);
            return;
        }

        {
            LOCK(pwalletMain->cs_wallet);
            pwalletMain->LockCoin(outpoint);
        }

        CCouncilorBroadcast mnb;
        std::string strError;
        if(!CCouncilorBroadcast::Create(outpoint, service, keyCollateral, pubKeyCollateral, keyCouncilor, pubKeyCouncilor, strError, mnb)) {
            nState = ACTIVE_COUNCILOR_NOT_CAPABLE;
            strNotCapableReason = "Error creating mastenode broadcast: " + strError;
            LogPrintf("CActiveCouncilor::ManageStateLocal -- %s: %s\n", GetStateString(), strNotCapableReason);
            return;
        }

        {
            LOCK(cs_main);
            // remember the hash of the block where councilor collateral had minimum required confirmations
            mnb.nCollateralMinConfBlockHash = chainActive[GetUTXOHeight(outpoint) + Params().GetConsensus().nCouncilorMinimumConfirmations - 1]->GetBlockHash();
        }

        fPingerEnabled = true;
        nState = ACTIVE_COUNCILOR_STARTED;

        //update to councilor list
        LogPrintf("CActiveCouncilor::ManageStateLocal -- Update Councilor List\n");
        mnodeman.UpdateCouncilorList(mnb, connman);
        mnodeman.NotifyCouncilorUpdates(connman);

        //send to all peers
        LogPrintf("CActiveCouncilor::ManageStateLocal -- Relay broadcast, collateral=%s\n", outpoint.ToStringShort());
        mnb.Relay(connman);
    }
}
