// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "activecouncilor.h"
#include "addrman.h"
#include "councilofricks.h"
#include "councilor-payments.h"
#include "councilor-sync.h"
#include "councilorman.h"
#include "messagesigner.h"
#include "netfulfilledman.h"
#include "privatesend-client.h"
#include "util.h"

/** Councilor manager */
CCouncilorMan mnodeman;

const std::string CCouncilorMan::SERIALIZATION_VERSION_STRING = "CCouncilorMan-Version-7";

struct CompareLastPaidBlock
{
    bool operator()(const std::pair<int, CCouncilor*>& t1,
                    const std::pair<int, CCouncilor*>& t2) const
    {
        return (t1.first != t2.first) ? (t1.first < t2.first) : (t1.second->vin < t2.second->vin);
    }
};

struct CompareScoreMN
{
    bool operator()(const std::pair<arith_uint256, CCouncilor*>& t1,
                    const std::pair<arith_uint256, CCouncilor*>& t2) const
    {
        return (t1.first != t2.first) ? (t1.first < t2.first) : (t1.second->vin < t2.second->vin);
    }
};

struct CompareByAddr

{
    bool operator()(const CCouncilor* t1,
                    const CCouncilor* t2) const
    {
        return t1->addr < t2->addr;
    }
};

CCouncilorMan::CCouncilorMan()
: cs(),
  mapCouncilors(),
  mAskedUsForCouncilorList(),
  mWeAskedForCouncilorList(),
  mWeAskedForCouncilorListEntry(),
  mWeAskedForVerification(),
  mMnbRecoveryRequests(),
  mMnbRecoveryGoodReplies(),
  listScheduledMnbRequestConnections(),
  fCouncilorsAdded(false),
  fCouncilorsRemoved(false),
  vecDirtyCouncilofRicksObjectHashes(),
  nLastWatchdogVoteTime(0),
  mapSeenCouncilorBroadcast(),
  mapSeenCouncilorPing(),
  nDsqCount(0)
{}

bool CCouncilorMan::Add(CCouncilor &mn)
{
    LOCK(cs);

    if (Has(mn.vin.prevout)) return false;

    LogPrint("councilor", "CCouncilorMan::Add -- Adding new Councilor: addr=%s, %i now\n", mn.addr.ToString(), size() + 1);
    mapCouncilors[mn.vin.prevout] = mn;
    fCouncilorsAdded = true;
    return true;
}

void CCouncilorMan::AskForMN(CNode* pnode, const COutPoint& outpoint, CConnman& connman)
{
    if(!pnode) return;

    LOCK(cs);

    std::map<COutPoint, std::map<CNetAddr, int64_t> >::iterator it1 = mWeAskedForCouncilorListEntry.find(outpoint);
    if (it1 != mWeAskedForCouncilorListEntry.end()) {
        std::map<CNetAddr, int64_t>::iterator it2 = it1->second.find(pnode->addr);
        if (it2 != it1->second.end()) {
            if (GetTime() < it2->second) {
                // we've asked recently, should not repeat too often or we could get banned
                return;
            }
            // we asked this node for this outpoint but it's ok to ask again already
            LogPrintf("CCouncilorMan::AskForMN -- Asking same peer %s for missing councilor entry again: %s\n", pnode->addr.ToString(), outpoint.ToStringShort());
        } else {
            // we already asked for this outpoint but not this node
            LogPrintf("CCouncilorMan::AskForMN -- Asking new peer %s for missing councilor entry: %s\n", pnode->addr.ToString(), outpoint.ToStringShort());
        }
    } else {
        // we never asked any node for this outpoint
        LogPrintf("CCouncilorMan::AskForMN -- Asking peer %s for missing councilor entry for the first time: %s\n", pnode->addr.ToString(), outpoint.ToStringShort());
    }
    mWeAskedForCouncilorListEntry[outpoint][pnode->addr] = GetTime() + DSEG_UPDATE_SECONDS;

    connman.PushMessage(pnode, NetMsgType::DSEG, CTxIn(outpoint));
}

bool CCouncilorMan::AllowMixing(const COutPoint &outpoint)
{
    LOCK(cs);
    CCouncilor* pmn = Find(outpoint);
    if (!pmn) {
        return false;
    }
    nDsqCount++;
    pmn->nLastDsq = nDsqCount;
    pmn->fAllowMixingTx = true;

    return true;
}

bool CCouncilorMan::DisallowMixing(const COutPoint &outpoint)
{
    LOCK(cs);
    CCouncilor* pmn = Find(outpoint);
    if (!pmn) {
        return false;
    }
    pmn->fAllowMixingTx = false;

    return true;
}

bool CCouncilorMan::PoSeBan(const COutPoint &outpoint)
{
    LOCK(cs);
    CCouncilor* pmn = Find(outpoint);
    if (!pmn) {
        return false;
    }
    pmn->PoSeBan();

    return true;
}

void CCouncilorMan::Check()
{
    LOCK(cs);

    LogPrint("councilor", "CCouncilorMan::Check -- nLastWatchdogVoteTime=%d, IsWatchdogActive()=%d\n", nLastWatchdogVoteTime, IsWatchdogActive());

    for (auto& mnpair : mapCouncilors) {
        mnpair.second.Check();
    }
}

void CCouncilorMan::CheckAndRemove(CConnman& connman)
{
    if(!councilorSync.IsCouncilorListSynced()) return;

    LogPrintf("CCouncilorMan::CheckAndRemove\n");

    {
        // Need LOCK2 here to ensure consistent locking order because code below locks cs_main
        // in CheckMnbAndUpdateCouncilorList()
        LOCK2(cs_main, cs);

        Check();

        // Remove spent councilors, prepare structures and make requests to reasure the state of inactive ones
        rank_pair_vec_t vecCouncilorRanks;
        // ask for up to MNB_RECOVERY_MAX_ASK_ENTRIES councilor entries at a time
        int nAskForMnbRecovery = MNB_RECOVERY_MAX_ASK_ENTRIES;
        std::map<COutPoint, CCouncilor>::iterator it = mapCouncilors.begin();
        while (it != mapCouncilors.end()) {
            CCouncilorBroadcast mnb = CCouncilorBroadcast(it->second);
            uint256 hash = mnb.GetHash();
            // If collateral was spent ...
            if (it->second.IsOutpointSpent()) {
                LogPrint("councilor", "CCouncilorMan::CheckAndRemove -- Removing Councilor: %s  addr=%s  %i now\n", it->second.GetStateString(), it->second.addr.ToString(), size() - 1);

                // erase all of the broadcasts we've seen from this txin, ...
                mapSeenCouncilorBroadcast.erase(hash);
                mWeAskedForCouncilorListEntry.erase(it->first);

                // and finally remove it from the list
                it->second.FlagCouncilofRicksItemsAsDirty();
                mapCouncilors.erase(it++);
                fCouncilorsRemoved = true;
            } else {
                bool fAsk = (nAskForMnbRecovery > 0) &&
                            councilorSync.IsSynced() &&
                            it->second.IsNewStartRequired() &&
                            !IsMnbRecoveryRequested(hash);
                if(fAsk) {
                    // this mn is in a non-recoverable state and we haven't asked other nodes yet
                    std::set<CNetAddr> setRequested;
                    // calulate only once and only when it's needed
                    if(vecCouncilorRanks.empty()) {
                        int nRandomBlockHeight = GetRandInt(nCachedBlockHeight);
                        GetCouncilorRanks(vecCouncilorRanks, nRandomBlockHeight);
                    }
                    bool fAskedForMnbRecovery = false;
                    // ask first MNB_RECOVERY_QUORUM_TOTAL councilors we can connect to and we haven't asked recently
                    for(int i = 0; setRequested.size() < MNB_RECOVERY_QUORUM_TOTAL && i < (int)vecCouncilorRanks.size(); i++) {
                        // avoid banning
                        if(mWeAskedForCouncilorListEntry.count(it->first) && mWeAskedForCouncilorListEntry[it->first].count(vecCouncilorRanks[i].second.addr)) continue;
                        // didn't ask recently, ok to ask now
                        CService addr = vecCouncilorRanks[i].second.addr;
                        setRequested.insert(addr);
                        listScheduledMnbRequestConnections.push_back(std::make_pair(addr, hash));
                        fAskedForMnbRecovery = true;
                    }
                    if(fAskedForMnbRecovery) {
                        LogPrint("councilor", "CCouncilorMan::CheckAndRemove -- Recovery initiated, councilor=%s\n", it->first.ToStringShort());
                        nAskForMnbRecovery--;
                    }
                    // wait for mnb recovery replies for MNB_RECOVERY_WAIT_SECONDS seconds
                    mMnbRecoveryRequests[hash] = std::make_pair(GetTime() + MNB_RECOVERY_WAIT_SECONDS, setRequested);
                }
                ++it;
            }
        }

        // proces replies for COUNCILOR_NEW_START_REQUIRED councilors
        LogPrint("councilor", "CCouncilorMan::CheckAndRemove -- mMnbRecoveryGoodReplies size=%d\n", (int)mMnbRecoveryGoodReplies.size());
        std::map<uint256, std::vector<CCouncilorBroadcast> >::iterator itMnbReplies = mMnbRecoveryGoodReplies.begin();
        while(itMnbReplies != mMnbRecoveryGoodReplies.end()){
            if(mMnbRecoveryRequests[itMnbReplies->first].first < GetTime()) {
                // all nodes we asked should have replied now
                if(itMnbReplies->second.size() >= MNB_RECOVERY_QUORUM_REQUIRED) {
                    // majority of nodes we asked agrees that this mn doesn't require new mnb, reprocess one of new mnbs
                    LogPrint("councilor", "CCouncilorMan::CheckAndRemove -- reprocessing mnb, councilor=%s\n", itMnbReplies->second[0].vin.prevout.ToStringShort());
                    // mapSeenCouncilorBroadcast.erase(itMnbReplies->first);
                    int nDos;
                    itMnbReplies->second[0].fRecovery = true;
                    CheckMnbAndUpdateCouncilorList(NULL, itMnbReplies->second[0], nDos, connman);
                }
                LogPrint("councilor", "CCouncilorMan::CheckAndRemove -- removing mnb recovery reply, councilor=%s, size=%d\n", itMnbReplies->second[0].vin.prevout.ToStringShort(), (int)itMnbReplies->second.size());
                mMnbRecoveryGoodReplies.erase(itMnbReplies++);
            } else {
                ++itMnbReplies;
            }
        }
    }
    {
        // no need for cm_main below
        LOCK(cs);

        std::map<uint256, std::pair< int64_t, std::set<CNetAddr> > >::iterator itMnbRequest = mMnbRecoveryRequests.begin();
        while(itMnbRequest != mMnbRecoveryRequests.end()){
            // Allow this mnb to be re-verified again after MNB_RECOVERY_RETRY_SECONDS seconds
            // if mn is still in COUNCILOR_NEW_START_REQUIRED state.
            if(GetTime() - itMnbRequest->second.first > MNB_RECOVERY_RETRY_SECONDS) {
                mMnbRecoveryRequests.erase(itMnbRequest++);
            } else {
                ++itMnbRequest;
            }
        }

        // check who's asked for the Councilor list
        std::map<CNetAddr, int64_t>::iterator it1 = mAskedUsForCouncilorList.begin();
        while(it1 != mAskedUsForCouncilorList.end()){
            if((*it1).second < GetTime()) {
                mAskedUsForCouncilorList.erase(it1++);
            } else {
                ++it1;
            }
        }

        // check who we asked for the Councilor list
        it1 = mWeAskedForCouncilorList.begin();
        while(it1 != mWeAskedForCouncilorList.end()){
            if((*it1).second < GetTime()){
                mWeAskedForCouncilorList.erase(it1++);
            } else {
                ++it1;
            }
        }

        // check which Councilors we've asked for
        std::map<COutPoint, std::map<CNetAddr, int64_t> >::iterator it2 = mWeAskedForCouncilorListEntry.begin();
        while(it2 != mWeAskedForCouncilorListEntry.end()){
            std::map<CNetAddr, int64_t>::iterator it3 = it2->second.begin();
            while(it3 != it2->second.end()){
                if(it3->second < GetTime()){
                    it2->second.erase(it3++);
                } else {
                    ++it3;
                }
            }
            if(it2->second.empty()) {
                mWeAskedForCouncilorListEntry.erase(it2++);
            } else {
                ++it2;
            }
        }

        std::map<CNetAddr, CCouncilorVerification>::iterator it3 = mWeAskedForVerification.begin();
        while(it3 != mWeAskedForVerification.end()){
            if(it3->second.nBlockHeight < nCachedBlockHeight - MAX_POSE_BLOCKS) {
                mWeAskedForVerification.erase(it3++);
            } else {
                ++it3;
            }
        }

        // NOTE: do not expire mapSeenCouncilorBroadcast entries here, clean them on mnb updates!

        // remove expired mapSeenCouncilorPing
        std::map<uint256, CCouncilorPing>::iterator it4 = mapSeenCouncilorPing.begin();
        while(it4 != mapSeenCouncilorPing.end()){
            if((*it4).second.IsExpired()) {
                LogPrint("councilor", "CCouncilorMan::CheckAndRemove -- Removing expired Councilor ping: hash=%s\n", (*it4).second.GetHash().ToString());
                mapSeenCouncilorPing.erase(it4++);
            } else {
                ++it4;
            }
        }

        // remove expired mapSeenCouncilorVerification
        std::map<uint256, CCouncilorVerification>::iterator itv2 = mapSeenCouncilorVerification.begin();
        while(itv2 != mapSeenCouncilorVerification.end()){
            if((*itv2).second.nBlockHeight < nCachedBlockHeight - MAX_POSE_BLOCKS){
                LogPrint("councilor", "CCouncilorMan::CheckAndRemove -- Removing expired Councilor verification: hash=%s\n", (*itv2).first.ToString());
                mapSeenCouncilorVerification.erase(itv2++);
            } else {
                ++itv2;
            }
        }

        LogPrintf("CCouncilorMan::CheckAndRemove -- %s\n", ToString());
    }

    if(fCouncilorsRemoved) {
        NotifyCouncilorUpdates(connman);
    }
}

void CCouncilorMan::Clear()
{
    LOCK(cs);
    mapCouncilors.clear();
    mAskedUsForCouncilorList.clear();
    mWeAskedForCouncilorList.clear();
    mWeAskedForCouncilorListEntry.clear();
    mapSeenCouncilorBroadcast.clear();
    mapSeenCouncilorPing.clear();
    nDsqCount = 0;
    nLastWatchdogVoteTime = 0;
}

int CCouncilorMan::CountCouncilors(int nProtocolVersion)
{
    LOCK(cs);
    int nCount = 0;
    nProtocolVersion = nProtocolVersion == -1 ? mnpayments.GetMinCouncilorPaymentsProto() : nProtocolVersion;

    for (auto& mnpair : mapCouncilors) {
        if(mnpair.second.nProtocolVersion < nProtocolVersion) continue;
        nCount++;
    }

    return nCount;
}

int CCouncilorMan::CountEnabled(int nProtocolVersion)
{
    LOCK(cs);
    int nCount = 0;
    nProtocolVersion = nProtocolVersion == -1 ? mnpayments.GetMinCouncilorPaymentsProto() : nProtocolVersion;

    for (auto& mnpair : mapCouncilors) {
        if(mnpair.second.nProtocolVersion < nProtocolVersion || !mnpair.second.IsEnabled()) continue;
        nCount++;
    }

    return nCount;
}

/* Only IPv4 councilors are allowed in 12.1, saving this for later
int CCouncilorMan::CountByIP(int nNetworkType)
{
    LOCK(cs);
    int nNodeCount = 0;

    for (auto& mnpair : mapCouncilors)
        if ((nNetworkType == NET_IPV4 && mnpair.second.addr.IsIPv4()) ||
            (nNetworkType == NET_TOR  && mnpair.second.addr.IsTor())  ||
            (nNetworkType == NET_IPV6 && mnpair.second.addr.IsIPv6())) {
                nNodeCount++;
        }

    return nNodeCount;
}
*/

void CCouncilorMan::DsegUpdate(CNode* pnode, CConnman& connman)
{
    LOCK(cs);

    if(Params().NetworkIDString() == CBaseChainParams::MAIN) {
        if(!(pnode->addr.IsRFC1918() || pnode->addr.IsLocal())) {
            std::map<CNetAddr, int64_t>::iterator it = mWeAskedForCouncilorList.find(pnode->addr);
            if(it != mWeAskedForCouncilorList.end() && GetTime() < (*it).second) {
                LogPrintf("CCouncilorMan::DsegUpdate -- we already asked %s for the list; skipping...\n", pnode->addr.ToString());
                return;
            }
        }
    }

    connman.PushMessage(pnode, NetMsgType::DSEG, CTxIn());
    int64_t askAgain = GetTime() + DSEG_UPDATE_SECONDS;
    mWeAskedForCouncilorList[pnode->addr] = askAgain;

    LogPrint("councilor", "CCouncilorMan::DsegUpdate -- asked %s for the list\n", pnode->addr.ToString());
}

CCouncilor* CCouncilorMan::Find(const COutPoint &outpoint)
{
    LOCK(cs);
    auto it = mapCouncilors.find(outpoint);
    return it == mapCouncilors.end() ? NULL : &(it->second);
}

bool CCouncilorMan::Get(const COutPoint& outpoint, CCouncilor& councilorRet)
{
    // Theses mutexes are recursive so double locking by the same thread is safe.
    LOCK(cs);
    auto it = mapCouncilors.find(outpoint);
    if (it == mapCouncilors.end()) {
        return false;
    }

    councilorRet = it->second;
    return true;
}

bool CCouncilorMan::GetCouncilorInfo(const COutPoint& outpoint, councilor_info_t& mnInfoRet)
{
    LOCK(cs);
    auto it = mapCouncilors.find(outpoint);
    if (it == mapCouncilors.end()) {
        return false;
    }
    mnInfoRet = it->second.GetInfo();
    return true;
}

bool CCouncilorMan::GetCouncilorInfo(const CPubKey& pubKeyCouncilor, councilor_info_t& mnInfoRet)
{
    LOCK(cs);
    for (auto& mnpair : mapCouncilors) {
        if (mnpair.second.pubKeyCouncilor == pubKeyCouncilor) {
            mnInfoRet = mnpair.second.GetInfo();
            return true;
        }
    }
    return false;
}

bool CCouncilorMan::GetCouncilorInfo(const CScript& payee, councilor_info_t& mnInfoRet)
{
    LOCK(cs);
    for (auto& mnpair : mapCouncilors) {
        CScript scriptCollateralAddress = GetScriptForDestination(mnpair.second.pubKeyCollateralAddress.GetID());
        if (scriptCollateralAddress == payee) {
            mnInfoRet = mnpair.second.GetInfo();
            return true;
        }
    }
    return false;
}

bool CCouncilorMan::Has(const COutPoint& outpoint)
{
    LOCK(cs);
    return mapCouncilors.find(outpoint) != mapCouncilors.end();
}

//
// Deterministically select the oldest/best councilor to pay on the network
//
bool CCouncilorMan::GetNextCouncilorInQueueForPayment(bool fFilterSigTime, int& nCountRet, councilor_info_t& mnInfoRet)
{
    return GetNextCouncilorInQueueForPayment(nCachedBlockHeight, fFilterSigTime, nCountRet, mnInfoRet);
}

bool CCouncilorMan::GetNextCouncilorInQueueForPayment(int nBlockHeight, bool fFilterSigTime, int& nCountRet, councilor_info_t& mnInfoRet)
{
    mnInfoRet = councilor_info_t();
    nCountRet = 0;

    if (!councilorSync.IsWinnersListSynced()) {
        // without winner list we can't reliably find the next winner anyway
        return false;
    }

    // Need LOCK2 here to ensure consistent locking order because the GetBlockHash call below locks cs_main
    LOCK2(cs_main,cs);

    std::vector<std::pair<int, CCouncilor*> > vecCouncilorLastPaid;

    /*
        Make a vector with all of the last paid times
    */

    int nMnCount = CountCouncilors();

    for (auto& mnpair : mapCouncilors) {
        if(!mnpair.second.IsValidForPayment()) continue;

        //check protocol version
        if(mnpair.second.nProtocolVersion < mnpayments.GetMinCouncilorPaymentsProto()) continue;

        //it's in the list (up to 8 entries ahead of current block to allow propagation) -- so let's skip it
        if(mnpayments.IsScheduled(mnpair.second, nBlockHeight)) continue;

        //it's too new, wait for a cycle
        if(fFilterSigTime && mnpair.second.sigTime + (nMnCount*2.6*60) > GetAdjustedTime()) continue;

        //make sure it has at least as many confirmations as there are councilors
        if(GetUTXOConfirmations(mnpair.first) < nMnCount) continue;

        vecCouncilorLastPaid.push_back(std::make_pair(mnpair.second.GetLastPaidBlock(), &mnpair.second));
    }

    nCountRet = (int)vecCouncilorLastPaid.size();

    //when the network is in the process of upgrading, don't penalize nodes that recently restarted
    if(fFilterSigTime && nCountRet < nMnCount/3)
        return GetNextCouncilorInQueueForPayment(nBlockHeight, false, nCountRet, mnInfoRet);

    // Sort them low to high
    sort(vecCouncilorLastPaid.begin(), vecCouncilorLastPaid.end(), CompareLastPaidBlock());

    uint256 blockHash;
    if(!GetBlockHash(blockHash, nBlockHeight - 101)) {
        LogPrintf("CCouncilor::GetNextCouncilorInQueueForPayment -- ERROR: GetBlockHash() failed at nBlockHeight %d\n", nBlockHeight - 101);
        return false;
    }
    // Look at 1/10 of the oldest nodes (by last payment), calculate their scores and pay the best one
    //  -- This doesn't look at who is being paid in the +8-10 blocks, allowing for double payments very rarely
    //  -- 1/100 payments should be a double payment on mainnet - (1/(3000/10))*2
    //  -- (chance per block * chances before IsScheduled will fire)
    int nTenthNetwork = nMnCount/10;
    int nCountTenth = 0;
    arith_uint256 nHighest = 0;
    CCouncilor *pBestCouncilor = NULL;
    BOOST_FOREACH (PAIRTYPE(int, CCouncilor*)& s, vecCouncilorLastPaid){
        arith_uint256 nScore = s.second->CalculateScore(blockHash);
        if(nScore > nHighest){
            nHighest = nScore;
            pBestCouncilor = s.second;
        }
        nCountTenth++;
        if(nCountTenth >= nTenthNetwork) break;
    }
    if (pBestCouncilor) {
        mnInfoRet = pBestCouncilor->GetInfo();
    }
    return mnInfoRet.fInfoValid;
}

councilor_info_t CCouncilorMan::FindRandomNotInVec(const std::vector<COutPoint> &vecToExclude, int nProtocolVersion)
{
    LOCK(cs);

    nProtocolVersion = nProtocolVersion == -1 ? mnpayments.GetMinCouncilorPaymentsProto() : nProtocolVersion;

    int nCountEnabled = CountEnabled(nProtocolVersion);
    int nCountNotExcluded = nCountEnabled - vecToExclude.size();

    LogPrintf("CCouncilorMan::FindRandomNotInVec -- %d enabled councilors, %d councilors to choose from\n", nCountEnabled, nCountNotExcluded);
    if(nCountNotExcluded < 1) return councilor_info_t();

    // fill a vector of pointers
    std::vector<CCouncilor*> vpCouncilorsShuffled;
    for (auto& mnpair : mapCouncilors) {
        vpCouncilorsShuffled.push_back(&mnpair.second);
    }

    InsecureRand insecureRand;
    // shuffle pointers
    std::random_shuffle(vpCouncilorsShuffled.begin(), vpCouncilorsShuffled.end(), insecureRand);
    bool fExclude;

    // loop through
    BOOST_FOREACH(CCouncilor* pmn, vpCouncilorsShuffled) {
        if(pmn->nProtocolVersion < nProtocolVersion || !pmn->IsEnabled()) continue;
        fExclude = false;
        BOOST_FOREACH(const COutPoint &outpointToExclude, vecToExclude) {
            if(pmn->vin.prevout == outpointToExclude) {
                fExclude = true;
                break;
            }
        }
        if(fExclude) continue;
        // found the one not in vecToExclude
        LogPrint("councilor", "CCouncilorMan::FindRandomNotInVec -- found, councilor=%s\n", pmn->vin.prevout.ToStringShort());
        return pmn->GetInfo();
    }

    LogPrint("councilor", "CCouncilorMan::FindRandomNotInVec -- failed\n");
    return councilor_info_t();
}

bool CCouncilorMan::GetCouncilorScores(const uint256& nBlockHash, CCouncilorMan::score_pair_vec_t& vecCouncilorScoresRet, int nMinProtocol)
{
    vecCouncilorScoresRet.clear();

    if (!councilorSync.IsCouncilorListSynced())
        return false;

    AssertLockHeld(cs);

    if (mapCouncilors.empty())
        return false;

    // calculate scores
    for (auto& mnpair : mapCouncilors) {
        if (mnpair.second.nProtocolVersion >= nMinProtocol) {
            vecCouncilorScoresRet.push_back(std::make_pair(mnpair.second.CalculateScore(nBlockHash), &mnpair.second));
        }
    }

    sort(vecCouncilorScoresRet.rbegin(), vecCouncilorScoresRet.rend(), CompareScoreMN());
    return !vecCouncilorScoresRet.empty();
}

bool CCouncilorMan::GetCouncilorRank(const COutPoint& outpoint, int& nRankRet, int nBlockHeight, int nMinProtocol)
{
    nRankRet = -1;

    if (!councilorSync.IsCouncilorListSynced())
        return false;

    // make sure we know about this block
    uint256 nBlockHash = uint256();
    if (!GetBlockHash(nBlockHash, nBlockHeight)) {
        LogPrintf("CCouncilorMan::%s -- ERROR: GetBlockHash() failed at nBlockHeight %d\n", __func__, nBlockHeight);
        return false;
    }

    LOCK(cs);

    score_pair_vec_t vecCouncilorScores;
    if (!GetCouncilorScores(nBlockHash, vecCouncilorScores, nMinProtocol))
        return false;

    int nRank = 0;
    for (auto& scorePair : vecCouncilorScores) {
        nRank++;
        if(scorePair.second->vin.prevout == outpoint) {
            nRankRet = nRank;
            return true;
        }
    }

    return false;
}

bool CCouncilorMan::GetCouncilorRanks(CCouncilorMan::rank_pair_vec_t& vecCouncilorRanksRet, int nBlockHeight, int nMinProtocol)
{
    vecCouncilorRanksRet.clear();

    if (!councilorSync.IsCouncilorListSynced())
        return false;

    // make sure we know about this block
    uint256 nBlockHash = uint256();
    if (!GetBlockHash(nBlockHash, nBlockHeight)) {
        LogPrintf("CCouncilorMan::%s -- ERROR: GetBlockHash() failed at nBlockHeight %d\n", __func__, nBlockHeight);
        return false;
    }

    LOCK(cs);

    score_pair_vec_t vecCouncilorScores;
    if (!GetCouncilorScores(nBlockHash, vecCouncilorScores, nMinProtocol))
        return false;

    int nRank = 0;
    for (auto& scorePair : vecCouncilorScores) {
        nRank++;
        vecCouncilorRanksRet.push_back(std::make_pair(nRank, *scorePair.second));
    }

    return true;
}

bool CCouncilorMan::GetCouncilorByRank(int nRankIn, councilor_info_t& mnInfoRet, int nBlockHeight, int nMinProtocol)
{
    mnInfoRet = councilor_info_t();

    if (!councilorSync.IsCouncilorListSynced())
        return false;

    // make sure we know about this block
    uint256 nBlockHash = uint256();
    if (!GetBlockHash(nBlockHash, nBlockHeight)) {
        LogPrintf("CCouncilorMan::%s -- ERROR: GetBlockHash() failed at nBlockHeight %d\n", __func__, nBlockHeight);
        return false;
    }

    LOCK(cs);

    score_pair_vec_t vecCouncilorScores;
    if (!GetCouncilorScores(nBlockHash, vecCouncilorScores, nMinProtocol))
        return false;

    if (vecCouncilorScores.size() < nRankIn)
        return false;

    int nRank = 0;
    for (auto& scorePair : vecCouncilorScores) {
        nRank++;
        if(nRank == nRankIn) {
            mnInfoRet = *scorePair.second;
            return true;
        }
    }

    return false;
}

void CCouncilorMan::ProcessCouncilorConnections(CConnman& connman)
{
    //we don't care about this for regtest
    if(Params().NetworkIDString() == CBaseChainParams::REGTEST) return;

    connman.ForEachNode(CConnman::AllNodes, [](CNode* pnode) {
        if(pnode->fCouncilor) {
            if(privateSendClient.infoMixingCouncilor.fInfoValid && pnode->addr == privateSendClient.infoMixingCouncilor.addr)
                return;
            LogPrintf("Closing Councilor connection: peer=%d, addr=%s\n", pnode->id, pnode->addr.ToString());
            pnode->fDisconnect = true;
        }
    });
}

std::pair<CService, std::set<uint256> > CCouncilorMan::PopScheduledMnbRequestConnection()
{
    LOCK(cs);
    if(listScheduledMnbRequestConnections.empty()) {
        return std::make_pair(CService(), std::set<uint256>());
    }

    std::set<uint256> setResult;

    listScheduledMnbRequestConnections.sort();
    std::pair<CService, uint256> pairFront = listScheduledMnbRequestConnections.front();

    // squash hashes from requests with the same CService as the first one into setResult
    std::list< std::pair<CService, uint256> >::iterator it = listScheduledMnbRequestConnections.begin();
    while(it != listScheduledMnbRequestConnections.end()) {
        if(pairFront.first == it->first) {
            setResult.insert(it->second);
            it = listScheduledMnbRequestConnections.erase(it);
        } else {
            // since list is sorted now, we can be sure that there is no more hashes left
            // to ask for from this addr
            break;
        }
    }
    return std::make_pair(pairFront.first, setResult);
}


void CCouncilorMan::ProcessMessage(CNode* pfrom, std::string& strCommand, CDataStream& vRecv, CConnman& connman)
{
    if(fLiteMode) return; // disable all Schmeckles specific functionality
    if(!councilorSync.IsBlockchainSynced()) return;

    if (strCommand == NetMsgType::MNANNOUNCE) { //Councilor Broadcast

        CCouncilorBroadcast mnb;
        vRecv >> mnb;

        pfrom->setAskFor.erase(mnb.GetHash());

        LogPrint("councilor", "MNANNOUNCE -- Councilor announce, councilor=%s\n", mnb.vin.prevout.ToStringShort());

        int nDos = 0;

        if (CheckMnbAndUpdateCouncilorList(pfrom, mnb, nDos, connman)) {
            // use announced Councilor as a peer
            connman.AddNewAddress(CAddress(mnb.addr, NODE_NETWORK), pfrom->addr, 2*60*60);
        } else if(nDos > 0) {
            Misbehaving(pfrom->GetId(), nDos);
        }

        if(fCouncilorsAdded) {
            NotifyCouncilorUpdates(connman);
        }
    } else if (strCommand == NetMsgType::MNPING) { //Councilor Ping

        CCouncilorPing mnp;
        vRecv >> mnp;

        uint256 nHash = mnp.GetHash();

        pfrom->setAskFor.erase(nHash);

        LogPrint("councilor", "MNPING -- Councilor ping, councilor=%s\n", mnp.vin.prevout.ToStringShort());

        // Need LOCK2 here to ensure consistent locking order because the CheckAndUpdate call below locks cs_main
        LOCK2(cs_main, cs);

        if(mapSeenCouncilorPing.count(nHash)) return; //seen
        mapSeenCouncilorPing.insert(std::make_pair(nHash, mnp));

        LogPrint("councilor", "MNPING -- Councilor ping, councilor=%s new\n", mnp.vin.prevout.ToStringShort());

        // see if we have this Councilor
        CCouncilor* pmn = Find(mnp.vin.prevout);

        // if councilor uses sentinel ping instead of watchdog
        // we shoud update nTimeLastWatchdogVote here if sentinel
        // ping flag is actual
        if(pmn && mnp.fSentinelIsCurrent)
            UpdateWatchdogVoteTime(mnp.vin.prevout, mnp.sigTime);

        // too late, new MNANNOUNCE is required
        if(pmn && pmn->IsNewStartRequired()) return;

        int nDos = 0;
        if(mnp.CheckAndUpdate(pmn, false, nDos, connman)) return;

        if(nDos > 0) {
            // if anything significant failed, mark that node
            Misbehaving(pfrom->GetId(), nDos);
        } else if(pmn != NULL) {
            // nothing significant failed, mn is a known one too
            return;
        }

        // something significant is broken or mn is unknown,
        // we might have to ask for a councilor entry once
        AskForMN(pfrom, mnp.vin.prevout, connman);

    } else if (strCommand == NetMsgType::DSEG) { //Get Councilor list or specific entry
        // Ignore such requests until we are fully synced.
        // We could start processing this after councilor list is synced
        // but this is a heavy one so it's better to finish sync first.
        if (!councilorSync.IsSynced()) return;

        CTxIn vin;
        vRecv >> vin;

        LogPrint("councilor", "DSEG -- Councilor list, councilor=%s\n", vin.prevout.ToStringShort());

        LOCK(cs);

        if(vin == CTxIn()) { //only should ask for this once
            //local network
            bool isLocal = (pfrom->addr.IsRFC1918() || pfrom->addr.IsLocal());

            if(!isLocal && Params().NetworkIDString() == CBaseChainParams::MAIN) {
                std::map<CNetAddr, int64_t>::iterator it = mAskedUsForCouncilorList.find(pfrom->addr);
                if (it != mAskedUsForCouncilorList.end() && it->second > GetTime()) {
                    Misbehaving(pfrom->GetId(), 34);
                    LogPrintf("DSEG -- peer already asked me for the list, peer=%d\n", pfrom->id);
                    return;
                }
                int64_t askAgain = GetTime() + DSEG_UPDATE_SECONDS;
                mAskedUsForCouncilorList[pfrom->addr] = askAgain;
            }
        } //else, asking for a specific node which is ok

        int nInvCount = 0;

        for (auto& mnpair : mapCouncilors) {
            if (vin != CTxIn() && vin != mnpair.second.vin) continue; // asked for specific vin but we are not there yet
            if (mnpair.second.addr.IsRFC1918() || mnpair.second.addr.IsLocal()) continue; // do not send local network councilor
            if (mnpair.second.IsUpdateRequired()) continue; // do not send outdated councilors

            LogPrint("councilor", "DSEG -- Sending Councilor entry: councilor=%s  addr=%s\n", mnpair.first.ToStringShort(), mnpair.second.addr.ToString());
            CCouncilorBroadcast mnb = CCouncilorBroadcast(mnpair.second);
            uint256 hash = mnb.GetHash();
            pfrom->PushInventory(CInv(MSG_COUNCILOR_ANNOUNCE, hash));
            pfrom->PushInventory(CInv(MSG_COUNCILOR_PING, mnpair.second.lastPing.GetHash()));
            nInvCount++;

            if (!mapSeenCouncilorBroadcast.count(hash)) {
                mapSeenCouncilorBroadcast.insert(std::make_pair(hash, std::make_pair(GetTime(), mnb)));
            }

            if (vin.prevout == mnpair.first) {
                LogPrintf("DSEG -- Sent 1 Councilor inv to peer %d\n", pfrom->id);
                return;
            }
        }

        if(vin == CTxIn()) {
            connman.PushMessage(pfrom, NetMsgType::SYNCSTATUSCOUNT, COUNCILOR_SYNC_LIST, nInvCount);
            LogPrintf("DSEG -- Sent %d Councilor invs to peer %d\n", nInvCount, pfrom->id);
            return;
        }
        // smth weird happen - someone asked us for vin we have no idea about?
        LogPrint("councilor", "DSEG -- No invs sent to peer %d\n", pfrom->id);

    } else if (strCommand == NetMsgType::MNVERIFY) { // Councilor Verify

        // Need LOCK2 here to ensure consistent locking order because the all functions below call GetBlockHash which locks cs_main
        LOCK2(cs_main, cs);

        CCouncilorVerification mnv;
        vRecv >> mnv;

        if(mnv.vchSig1.empty()) {
            // CASE 1: someone asked me to verify myself /IP we are using/
            SendVerifyReply(pfrom, mnv, connman);
        } else if (mnv.vchSig2.empty()) {
            // CASE 2: we _probably_ got verification we requested from some councilor
            ProcessVerifyReply(pfrom, mnv);
        } else {
            // CASE 3: we _probably_ got verification broadcast signed by some councilor which verified another one
            ProcessVerifyBroadcast(pfrom, mnv);
        }
    }
}

// Verification of councilors via unique direct requests.

void CCouncilorMan::DoFullVerificationStep(CConnman& connman)
{
    if(activeCouncilor.outpoint == COutPoint()) return;
    if(!councilorSync.IsSynced()) return;

    rank_pair_vec_t vecCouncilorRanks;
    GetCouncilorRanks(vecCouncilorRanks, nCachedBlockHeight - 1, MIN_POSE_PROTO_VERSION);

    // Need LOCK2 here to ensure consistent locking order because the SendVerifyRequest call below locks cs_main
    // through GetHeight() signal in ConnectNode
    LOCK2(cs_main, cs);

    int nCount = 0;

    int nMyRank = -1;
    int nRanksTotal = (int)vecCouncilorRanks.size();

    // send verify requests only if we are in top MAX_POSE_RANK
    std::vector<std::pair<int, CCouncilor> >::iterator it = vecCouncilorRanks.begin();
    while(it != vecCouncilorRanks.end()) {
        if(it->first > MAX_POSE_RANK) {
            LogPrint("councilor", "CCouncilorMan::DoFullVerificationStep -- Must be in top %d to send verify request\n",
                        (int)MAX_POSE_RANK);
            return;
        }
        if(it->second.vin.prevout == activeCouncilor.outpoint) {
            nMyRank = it->first;
            LogPrint("councilor", "CCouncilorMan::DoFullVerificationStep -- Found self at rank %d/%d, verifying up to %d councilors\n",
                        nMyRank, nRanksTotal, (int)MAX_POSE_CONNECTIONS);
            break;
        }
        ++it;
    }

    // edge case: list is too short and this councilor is not enabled
    if(nMyRank == -1) return;

    // send verify requests to up to MAX_POSE_CONNECTIONS councilors
    // starting from MAX_POSE_RANK + nMyRank and using MAX_POSE_CONNECTIONS as a step
    int nOffset = MAX_POSE_RANK + nMyRank - 1;
    if(nOffset >= (int)vecCouncilorRanks.size()) return;

    std::vector<CCouncilor*> vSortedByAddr;
    for (auto& mnpair : mapCouncilors) {
        vSortedByAddr.push_back(&mnpair.second);
    }

    sort(vSortedByAddr.begin(), vSortedByAddr.end(), CompareByAddr());

    it = vecCouncilorRanks.begin() + nOffset;
    while(it != vecCouncilorRanks.end()) {
        if(it->second.IsPoSeVerified() || it->second.IsPoSeBanned()) {
            LogPrint("councilor", "CCouncilorMan::DoFullVerificationStep -- Already %s%s%s councilor %s address %s, skipping...\n",
                        it->second.IsPoSeVerified() ? "verified" : "",
                        it->second.IsPoSeVerified() && it->second.IsPoSeBanned() ? " and " : "",
                        it->second.IsPoSeBanned() ? "banned" : "",
                        it->second.vin.prevout.ToStringShort(), it->second.addr.ToString());
            nOffset += MAX_POSE_CONNECTIONS;
            if(nOffset >= (int)vecCouncilorRanks.size()) break;
            it += MAX_POSE_CONNECTIONS;
            continue;
        }
        LogPrint("councilor", "CCouncilorMan::DoFullVerificationStep -- Verifying councilor %s rank %d/%d address %s\n",
                    it->second.vin.prevout.ToStringShort(), it->first, nRanksTotal, it->second.addr.ToString());
        if(SendVerifyRequest(CAddress(it->second.addr, NODE_NETWORK), vSortedByAddr, connman)) {
            nCount++;
            if(nCount >= MAX_POSE_CONNECTIONS) break;
        }
        nOffset += MAX_POSE_CONNECTIONS;
        if(nOffset >= (int)vecCouncilorRanks.size()) break;
        it += MAX_POSE_CONNECTIONS;
    }

    LogPrint("councilor", "CCouncilorMan::DoFullVerificationStep -- Sent verification requests to %d councilors\n", nCount);
}

// This function tries to find councilors with the same addr,
// find a verified one and ban all the other. If there are many nodes
// with the same addr but none of them is verified yet, then none of them are banned.
// It could take many times to run this before most of the duplicate nodes are banned.

void CCouncilorMan::CheckSameAddr()
{
    if(!councilorSync.IsSynced() || mapCouncilors.empty()) return;

    std::vector<CCouncilor*> vBan;
    std::vector<CCouncilor*> vSortedByAddr;

    {
        LOCK(cs);

        CCouncilor* pprevCouncilor = NULL;
        CCouncilor* pverifiedCouncilor = NULL;

        for (auto& mnpair : mapCouncilors) {
            vSortedByAddr.push_back(&mnpair.second);
        }

        sort(vSortedByAddr.begin(), vSortedByAddr.end(), CompareByAddr());

        BOOST_FOREACH(CCouncilor* pmn, vSortedByAddr) {
            // check only (pre)enabled councilors
            if(!pmn->IsEnabled() && !pmn->IsPreEnabled()) continue;
            // initial step
            if(!pprevCouncilor) {
                pprevCouncilor = pmn;
                pverifiedCouncilor = pmn->IsPoSeVerified() ? pmn : NULL;
                continue;
            }
            // second+ step
            if(pmn->addr == pprevCouncilor->addr) {
                if(pverifiedCouncilor) {
                    // another councilor with the same ip is verified, ban this one
                    vBan.push_back(pmn);
                } else if(pmn->IsPoSeVerified()) {
                    // this councilor with the same ip is verified, ban previous one
                    vBan.push_back(pprevCouncilor);
                    // and keep a reference to be able to ban following councilors with the same ip
                    pverifiedCouncilor = pmn;
                }
            } else {
                pverifiedCouncilor = pmn->IsPoSeVerified() ? pmn : NULL;
            }
            pprevCouncilor = pmn;
        }
    }

    // ban duplicates
    BOOST_FOREACH(CCouncilor* pmn, vBan) {
        LogPrintf("CCouncilorMan::CheckSameAddr -- increasing PoSe ban score for councilor %s\n", pmn->vin.prevout.ToStringShort());
        pmn->IncreasePoSeBanScore();
    }
}

bool CCouncilorMan::SendVerifyRequest(const CAddress& addr, const std::vector<CCouncilor*>& vSortedByAddr, CConnman& connman)
{
    if(netfulfilledman.HasFulfilledRequest(addr, strprintf("%s", NetMsgType::MNVERIFY)+"-request")) {
        // we already asked for verification, not a good idea to do this too often, skip it
        LogPrint("councilor", "CCouncilorMan::SendVerifyRequest -- too many requests, skipping... addr=%s\n", addr.ToString());
        return false;
    }

    CNode* pnode = connman.ConnectNode(addr, NULL, true);
    if(pnode == NULL) {
        LogPrintf("CCouncilorMan::SendVerifyRequest -- can't connect to node to verify it, addr=%s\n", addr.ToString());
        return false;
    }

    netfulfilledman.AddFulfilledRequest(addr, strprintf("%s", NetMsgType::MNVERIFY)+"-request");
    // use random nonce, store it and require node to reply with correct one later
    CCouncilorVerification mnv(addr, GetRandInt(999999), nCachedBlockHeight - 1);
    mWeAskedForVerification[addr] = mnv;
    LogPrintf("CCouncilorMan::SendVerifyRequest -- verifying node using nonce %d addr=%s\n", mnv.nonce, addr.ToString());
    connman.PushMessage(pnode, NetMsgType::MNVERIFY, mnv);

    return true;
}

void CCouncilorMan::SendVerifyReply(CNode* pnode, CCouncilorVerification& mnv, CConnman& connman)
{
    // only councilors can sign this, why would someone ask regular node?
    if(!fCounciLor) {
        // do not ban, malicious node might be using my IP
        // and trying to confuse the node which tries to verify it
        return;
    }

    if(netfulfilledman.HasFulfilledRequest(pnode->addr, strprintf("%s", NetMsgType::MNVERIFY)+"-reply")) {
        // peer should not ask us that often
        LogPrintf("CouncilorMan::SendVerifyReply -- ERROR: peer already asked me recently, peer=%d\n", pnode->id);
        Misbehaving(pnode->id, 20);
        return;
    }

    uint256 blockHash;
    if(!GetBlockHash(blockHash, mnv.nBlockHeight)) {
        LogPrintf("CouncilorMan::SendVerifyReply -- can't get block hash for unknown block height %d, peer=%d\n", mnv.nBlockHeight, pnode->id);
        return;
    }

    std::string strMessage = strprintf("%s%d%s", activeCouncilor.service.ToString(false), mnv.nonce, blockHash.ToString());

    if(!CMessageSigner::SignMessage(strMessage, mnv.vchSig1, activeCouncilor.keyCouncilor)) {
        LogPrintf("CouncilorMan::SendVerifyReply -- SignMessage() failed\n");
        return;
    }

    std::string strError;

    if(!CMessageSigner::VerifyMessage(activeCouncilor.pubKeyCouncilor, mnv.vchSig1, strMessage, strError)) {
        LogPrintf("CouncilorMan::SendVerifyReply -- VerifyMessage() failed, error: %s\n", strError);
        return;
    }

    connman.PushMessage(pnode, NetMsgType::MNVERIFY, mnv);
    netfulfilledman.AddFulfilledRequest(pnode->addr, strprintf("%s", NetMsgType::MNVERIFY)+"-reply");
}

void CCouncilorMan::ProcessVerifyReply(CNode* pnode, CCouncilorVerification& mnv)
{
    std::string strError;

    // did we even ask for it? if that's the case we should have matching fulfilled request
    if(!netfulfilledman.HasFulfilledRequest(pnode->addr, strprintf("%s", NetMsgType::MNVERIFY)+"-request")) {
        LogPrintf("CCouncilorMan::ProcessVerifyReply -- ERROR: we didn't ask for verification of %s, peer=%d\n", pnode->addr.ToString(), pnode->id);
        Misbehaving(pnode->id, 20);
        return;
    }

    // Received nonce for a known address must match the one we sent
    if(mWeAskedForVerification[pnode->addr].nonce != mnv.nonce) {
        LogPrintf("CCouncilorMan::ProcessVerifyReply -- ERROR: wrong nounce: requested=%d, received=%d, peer=%d\n",
                    mWeAskedForVerification[pnode->addr].nonce, mnv.nonce, pnode->id);
        Misbehaving(pnode->id, 20);
        return;
    }

    // Received nBlockHeight for a known address must match the one we sent
    if(mWeAskedForVerification[pnode->addr].nBlockHeight != mnv.nBlockHeight) {
        LogPrintf("CCouncilorMan::ProcessVerifyReply -- ERROR: wrong nBlockHeight: requested=%d, received=%d, peer=%d\n",
                    mWeAskedForVerification[pnode->addr].nBlockHeight, mnv.nBlockHeight, pnode->id);
        Misbehaving(pnode->id, 20);
        return;
    }

    uint256 blockHash;
    if(!GetBlockHash(blockHash, mnv.nBlockHeight)) {
        // this shouldn't happen...
        LogPrintf("CouncilorMan::ProcessVerifyReply -- can't get block hash for unknown block height %d, peer=%d\n", mnv.nBlockHeight, pnode->id);
        return;
    }

    // we already verified this address, why node is spamming?
    if(netfulfilledman.HasFulfilledRequest(pnode->addr, strprintf("%s", NetMsgType::MNVERIFY)+"-done")) {
        LogPrintf("CCouncilorMan::ProcessVerifyReply -- ERROR: already verified %s recently\n", pnode->addr.ToString());
        Misbehaving(pnode->id, 20);
        return;
    }

    {
        LOCK(cs);

        CCouncilor* prealCouncilor = NULL;
        std::vector<CCouncilor*> vpCouncilorsToBan;
        std::string strMessage1 = strprintf("%s%d%s", pnode->addr.ToString(false), mnv.nonce, blockHash.ToString());
        for (auto& mnpair : mapCouncilors) {
            if(CAddress(mnpair.second.addr, NODE_NETWORK) == pnode->addr) {
                if(CMessageSigner::VerifyMessage(mnpair.second.pubKeyCouncilor, mnv.vchSig1, strMessage1, strError)) {
                    // found it!
                    prealCouncilor = &mnpair.second;
                    if(!mnpair.second.IsPoSeVerified()) {
                        mnpair.second.DecreasePoSeBanScore();
                    }
                    netfulfilledman.AddFulfilledRequest(pnode->addr, strprintf("%s", NetMsgType::MNVERIFY)+"-done");

                    // we can only broadcast it if we are an activated councilor
                    if(activeCouncilor.outpoint == COutPoint()) continue;
                    // update ...
                    mnv.addr = mnpair.second.addr;
                    mnv.vin1 = mnpair.second.vin;
                    mnv.vin2 = CTxIn(activeCouncilor.outpoint);
                    std::string strMessage2 = strprintf("%s%d%s%s%s", mnv.addr.ToString(false), mnv.nonce, blockHash.ToString(),
                                            mnv.vin1.prevout.ToStringShort(), mnv.vin2.prevout.ToStringShort());
                    // ... and sign it
                    if(!CMessageSigner::SignMessage(strMessage2, mnv.vchSig2, activeCouncilor.keyCouncilor)) {
                        LogPrintf("CouncilorMan::ProcessVerifyReply -- SignMessage() failed\n");
                        return;
                    }

                    std::string strError;

                    if(!CMessageSigner::VerifyMessage(activeCouncilor.pubKeyCouncilor, mnv.vchSig2, strMessage2, strError)) {
                        LogPrintf("CouncilorMan::ProcessVerifyReply -- VerifyMessage() failed, error: %s\n", strError);
                        return;
                    }

                    mWeAskedForVerification[pnode->addr] = mnv;
                    mnv.Relay();

                } else {
                    vpCouncilorsToBan.push_back(&mnpair.second);
                }
            }
        }
        // no real councilor found?...
        if(!prealCouncilor) {
            // this should never be the case normally,
            // only if someone is trying to game the system in some way or smth like that
            LogPrintf("CCouncilorMan::ProcessVerifyReply -- ERROR: no real councilor found for addr %s\n", pnode->addr.ToString());
            Misbehaving(pnode->id, 20);
            return;
        }
        LogPrintf("CCouncilorMan::ProcessVerifyReply -- verified real councilor %s for addr %s\n",
                    prealCouncilor->vin.prevout.ToStringShort(), pnode->addr.ToString());
        // increase ban score for everyone else
        BOOST_FOREACH(CCouncilor* pmn, vpCouncilorsToBan) {
            pmn->IncreasePoSeBanScore();
            LogPrint("councilor", "CCouncilorMan::ProcessVerifyBroadcast -- increased PoSe ban score for %s addr %s, new score %d\n",
                        prealCouncilor->vin.prevout.ToStringShort(), pnode->addr.ToString(), pmn->nPoSeBanScore);
        }
        LogPrintf("CCouncilorMan::ProcessVerifyBroadcast -- PoSe score increased for %d fake councilors, addr %s\n",
                    (int)vpCouncilorsToBan.size(), pnode->addr.ToString());
    }
}

void CCouncilorMan::ProcessVerifyBroadcast(CNode* pnode, const CCouncilorVerification& mnv)
{
    std::string strError;

    if(mapSeenCouncilorVerification.find(mnv.GetHash()) != mapSeenCouncilorVerification.end()) {
        // we already have one
        return;
    }
    mapSeenCouncilorVerification[mnv.GetHash()] = mnv;

    // we don't care about history
    if(mnv.nBlockHeight < nCachedBlockHeight - MAX_POSE_BLOCKS) {
        LogPrint("councilor", "CCouncilorMan::ProcessVerifyBroadcast -- Outdated: current block %d, verification block %d, peer=%d\n",
                    nCachedBlockHeight, mnv.nBlockHeight, pnode->id);
        return;
    }

    if(mnv.vin1.prevout == mnv.vin2.prevout) {
        LogPrint("councilor", "CCouncilorMan::ProcessVerifyBroadcast -- ERROR: same vins %s, peer=%d\n",
                    mnv.vin1.prevout.ToStringShort(), pnode->id);
        // that was NOT a good idea to cheat and verify itself,
        // ban the node we received such message from
        Misbehaving(pnode->id, 100);
        return;
    }

    uint256 blockHash;
    if(!GetBlockHash(blockHash, mnv.nBlockHeight)) {
        // this shouldn't happen...
        LogPrintf("CCouncilorMan::ProcessVerifyBroadcast -- Can't get block hash for unknown block height %d, peer=%d\n", mnv.nBlockHeight, pnode->id);
        return;
    }

    int nRank;

    if (!GetCouncilorRank(mnv.vin2.prevout, nRank, mnv.nBlockHeight, MIN_POSE_PROTO_VERSION)) {
        LogPrint("councilor", "CCouncilorMan::ProcessVerifyBroadcast -- Can't calculate rank for councilor %s\n",
                    mnv.vin2.prevout.ToStringShort());
        return;
    }

    if(nRank > MAX_POSE_RANK) {
        LogPrint("councilor", "CCouncilorMan::ProcessVerifyBroadcast -- Councilor %s is not in top %d, current rank %d, peer=%d\n",
                    mnv.vin2.prevout.ToStringShort(), (int)MAX_POSE_RANK, nRank, pnode->id);
        return;
    }

    {
        LOCK(cs);

        std::string strMessage1 = strprintf("%s%d%s", mnv.addr.ToString(false), mnv.nonce, blockHash.ToString());
        std::string strMessage2 = strprintf("%s%d%s%s%s", mnv.addr.ToString(false), mnv.nonce, blockHash.ToString(),
                                mnv.vin1.prevout.ToStringShort(), mnv.vin2.prevout.ToStringShort());

        CCouncilor* pmn1 = Find(mnv.vin1.prevout);
        if(!pmn1) {
            LogPrintf("CCouncilorMan::ProcessVerifyBroadcast -- can't find councilor1 %s\n", mnv.vin1.prevout.ToStringShort());
            return;
        }

        CCouncilor* pmn2 = Find(mnv.vin2.prevout);
        if(!pmn2) {
            LogPrintf("CCouncilorMan::ProcessVerifyBroadcast -- can't find councilor2 %s\n", mnv.vin2.prevout.ToStringShort());
            return;
        }

        if(pmn1->addr != mnv.addr) {
            LogPrintf("CCouncilorMan::ProcessVerifyBroadcast -- addr %s do not match %s\n", mnv.addr.ToString(), pnode->addr.ToString());
            return;
        }

        if(CMessageSigner::VerifyMessage(pmn1->pubKeyCouncilor, mnv.vchSig1, strMessage1, strError)) {
            LogPrintf("CCouncilorMan::ProcessVerifyBroadcast -- VerifyMessage() for councilor1 failed, error: %s\n", strError);
            return;
        }

        if(CMessageSigner::VerifyMessage(pmn2->pubKeyCouncilor, mnv.vchSig2, strMessage2, strError)) {
            LogPrintf("CCouncilorMan::ProcessVerifyBroadcast -- VerifyMessage() for councilor2 failed, error: %s\n", strError);
            return;
        }

        if(!pmn1->IsPoSeVerified()) {
            pmn1->DecreasePoSeBanScore();
        }
        mnv.Relay();

        LogPrintf("CCouncilorMan::ProcessVerifyBroadcast -- verified councilor %s for addr %s\n",
                    pmn1->vin.prevout.ToStringShort(), pnode->addr.ToString());

        // increase ban score for everyone else with the same addr
        int nCount = 0;
        for (auto& mnpair : mapCouncilors) {
            if(mnpair.second.addr != mnv.addr || mnpair.first == mnv.vin1.prevout) continue;
            mnpair.second.IncreasePoSeBanScore();
            nCount++;
            LogPrint("councilor", "CCouncilorMan::ProcessVerifyBroadcast -- increased PoSe ban score for %s addr %s, new score %d\n",
                        mnpair.first.ToStringShort(), mnpair.second.addr.ToString(), mnpair.second.nPoSeBanScore);
        }
        LogPrintf("CCouncilorMan::ProcessVerifyBroadcast -- PoSe score incresed for %d fake councilors, addr %s\n",
                    nCount, pnode->addr.ToString());
    }
}

std::string CCouncilorMan::ToString() const
{
    std::ostringstream info;

    info << "Councilors: " << (int)mapCouncilors.size() <<
            ", peers who asked us for Councilor list: " << (int)mAskedUsForCouncilorList.size() <<
            ", peers we asked for Councilor list: " << (int)mWeAskedForCouncilorList.size() <<
            ", entries in Councilor list we asked for: " << (int)mWeAskedForCouncilorListEntry.size() <<
            ", nDsqCount: " << (int)nDsqCount;

    return info.str();
}

void CCouncilorMan::UpdateCouncilorList(CCouncilorBroadcast mnb, CConnman& connman)
{
    LOCK2(cs_main, cs);
    mapSeenCouncilorPing.insert(std::make_pair(mnb.lastPing.GetHash(), mnb.lastPing));
    mapSeenCouncilorBroadcast.insert(std::make_pair(mnb.GetHash(), std::make_pair(GetTime(), mnb)));

    LogPrintf("CCouncilorMan::UpdateCouncilorList -- councilor=%s  addr=%s\n", mnb.vin.prevout.ToStringShort(), mnb.addr.ToString());

    CCouncilor* pmn = Find(mnb.vin.prevout);
    if(pmn == NULL) {
        if(Add(mnb)) {
            councilorSync.BumpAssetLastTime("CCouncilorMan::UpdateCouncilorList - new");
        }
    } else {
        CCouncilorBroadcast mnbOld = mapSeenCouncilorBroadcast[CCouncilorBroadcast(*pmn).GetHash()].second;
        if(pmn->UpdateFromNewBroadcast(mnb, connman)) {
            councilorSync.BumpAssetLastTime("CCouncilorMan::UpdateCouncilorList - seen");
            mapSeenCouncilorBroadcast.erase(mnbOld.GetHash());
        }
    }
}

bool CCouncilorMan::CheckMnbAndUpdateCouncilorList(CNode* pfrom, CCouncilorBroadcast mnb, int& nDos, CConnman& connman)
{
    // Need to lock cs_main here to ensure consistent locking order because the SimpleCheck call below locks cs_main
    LOCK(cs_main);

    {
        LOCK(cs);
        nDos = 0;
        LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- councilor=%s\n", mnb.vin.prevout.ToStringShort());

        uint256 hash = mnb.GetHash();
        if(mapSeenCouncilorBroadcast.count(hash) && !mnb.fRecovery) { //seen
            LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- councilor=%s seen\n", mnb.vin.prevout.ToStringShort());
            // less then 2 pings left before this MN goes into non-recoverable state, bump sync timeout
            if(GetTime() - mapSeenCouncilorBroadcast[hash].first > COUNCILOR_NEW_START_REQUIRED_SECONDS - COUNCILOR_MIN_MNP_SECONDS * 2) {
                LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- councilor=%s seen update\n", mnb.vin.prevout.ToStringShort());
                mapSeenCouncilorBroadcast[hash].first = GetTime();
                councilorSync.BumpAssetLastTime("CCouncilorMan::CheckMnbAndUpdateCouncilorList - seen");
            }
            // did we ask this node for it?
            if(pfrom && IsMnbRecoveryRequested(hash) && GetTime() < mMnbRecoveryRequests[hash].first) {
                LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- mnb=%s seen request\n", hash.ToString());
                if(mMnbRecoveryRequests[hash].second.count(pfrom->addr)) {
                    LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- mnb=%s seen request, addr=%s\n", hash.ToString(), pfrom->addr.ToString());
                    // do not allow node to send same mnb multiple times in recovery mode
                    mMnbRecoveryRequests[hash].second.erase(pfrom->addr);
                    // does it have newer lastPing?
                    if(mnb.lastPing.sigTime > mapSeenCouncilorBroadcast[hash].second.lastPing.sigTime) {
                        // simulate Check
                        CCouncilor mnTemp = CCouncilor(mnb);
                        mnTemp.Check();
                        LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- mnb=%s seen request, addr=%s, better lastPing: %d min ago, projected mn state: %s\n", hash.ToString(), pfrom->addr.ToString(), (GetAdjustedTime() - mnb.lastPing.sigTime)/60, mnTemp.GetStateString());
                        if(mnTemp.IsValidStateForAutoStart(mnTemp.nActiveState)) {
                            // this node thinks it's a good one
                            LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- councilor=%s seen good\n", mnb.vin.prevout.ToStringShort());
                            mMnbRecoveryGoodReplies[hash].push_back(mnb);
                        }
                    }
                }
            }
            return true;
        }
        mapSeenCouncilorBroadcast.insert(std::make_pair(hash, std::make_pair(GetTime(), mnb)));

        LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- councilor=%s new\n", mnb.vin.prevout.ToStringShort());

        if(!mnb.SimpleCheck(nDos)) {
            LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- SimpleCheck() failed, councilor=%s\n", mnb.vin.prevout.ToStringShort());
            return false;
        }

        // search Councilor list
        CCouncilor* pmn = Find(mnb.vin.prevout);
        if(pmn) {
            CCouncilorBroadcast mnbOld = mapSeenCouncilorBroadcast[CCouncilorBroadcast(*pmn).GetHash()].second;
            if(!mnb.Update(pmn, nDos, connman)) {
                LogPrint("councilor", "CCouncilorMan::CheckMnbAndUpdateCouncilorList -- Update() failed, councilor=%s\n", mnb.vin.prevout.ToStringShort());
                return false;
            }
            if(hash != mnbOld.GetHash()) {
                mapSeenCouncilorBroadcast.erase(mnbOld.GetHash());
            }
            return true;
        }
    }

    if(mnb.CheckOutpoint(nDos)) {
        Add(mnb);
        councilorSync.BumpAssetLastTime("CCouncilorMan::CheckMnbAndUpdateCouncilorList - new");
        // if it matches our Councilor privkey...
        if(fCounciLor && mnb.pubKeyCouncilor == activeCouncilor.pubKeyCouncilor) {
            mnb.nPoSeBanScore = -COUNCILOR_POSE_BAN_MAX_SCORE;
            if(mnb.nProtocolVersion == PROTOCOL_VERSION) {
                // ... and PROTOCOL_VERSION, then we've been remotely activated ...
                LogPrintf("CCouncilorMan::CheckMnbAndUpdateCouncilorList -- Got NEW Councilor entry: councilor=%s  sigTime=%lld  addr=%s\n",
                            mnb.vin.prevout.ToStringShort(), mnb.sigTime, mnb.addr.ToString());
                activeCouncilor.ManageState(connman);
            } else {
                // ... otherwise we need to reactivate our node, do not add it to the list and do not relay
                // but also do not ban the node we get this message from
                LogPrintf("CCouncilorMan::CheckMnbAndUpdateCouncilorList -- wrong PROTOCOL_VERSION, re-activate your MN: message nProtocolVersion=%d  PROTOCOL_VERSION=%d\n", mnb.nProtocolVersion, PROTOCOL_VERSION);
                return false;
            }
        }
        mnb.Relay(connman);
    } else {
        LogPrintf("CCouncilorMan::CheckMnbAndUpdateCouncilorList -- Rejected Councilor entry: %s  addr=%s\n", mnb.vin.prevout.ToStringShort(), mnb.addr.ToString());
        return false;
    }

    return true;
}

void CCouncilorMan::UpdateLastPaid(const CBlockIndex* pindex)
{
    LOCK(cs);

    if(fLiteMode || !councilorSync.IsWinnersListSynced() || mapCouncilors.empty()) return;

    static bool IsFirstRun = true;
    // Do full scan on first run or if we are not a councilor
    // (MNs should update this info on every block, so limited scan should be enough for them)
    int nMaxBlocksToScanBack = (IsFirstRun || !fCounciLor) ? mnpayments.GetStorageLimit() : LAST_PAID_SCAN_BLOCKS;

    // LogPrint("mnpayments", "CCouncilorMan::UpdateLastPaid -- nHeight=%d, nMaxBlocksToScanBack=%d, IsFirstRun=%s\n",
    //                         nCachedBlockHeight, nMaxBlocksToScanBack, IsFirstRun ? "true" : "false");

    for (auto& mnpair: mapCouncilors) {
        mnpair.second.UpdateLastPaid(pindex, nMaxBlocksToScanBack);
    }

    IsFirstRun = false;
}

void CCouncilorMan::UpdateWatchdogVoteTime(const COutPoint& outpoint, uint64_t nVoteTime)
{
    LOCK(cs);
    CCouncilor* pmn = Find(outpoint);
    if(!pmn) {
        return;
    }
    pmn->UpdateWatchdogVoteTime(nVoteTime);
    nLastWatchdogVoteTime = GetTime();
}

bool CCouncilorMan::IsWatchdogActive()
{
    LOCK(cs);
    // Check if any councilors have voted recently, otherwise return false
    return (GetTime() - nLastWatchdogVoteTime) <= COUNCILOR_WATCHDOG_MAX_SECONDS;
}

bool CCouncilorMan::AddCouncilofRicksVote(const COutPoint& outpoint, uint256 nCouncilofRicksObjectHash)
{
    LOCK(cs);
    CCouncilor* pmn = Find(outpoint);
    if(!pmn) {
        return false;
    }
    pmn->AddCouncilofRicksVote(nCouncilofRicksObjectHash);
    return true;
}

void CCouncilorMan::RemoveCouncilofRicksObject(uint256 nCouncilofRicksObjectHash)
{
    LOCK(cs);
    for(auto& mnpair : mapCouncilors) {
        mnpair.second.RemoveCouncilofRicksObject(nCouncilofRicksObjectHash);
    }
}

void CCouncilorMan::CheckCouncilor(const CPubKey& pubKeyCouncilor, bool fForce)
{
    LOCK(cs);
    for (auto& mnpair : mapCouncilors) {
        if (mnpair.second.pubKeyCouncilor == pubKeyCouncilor) {
            mnpair.second.Check(fForce);
            return;
        }
    }
}

bool CCouncilorMan::IsCouncilorPingedWithin(const COutPoint& outpoint, int nSeconds, int64_t nTimeToCheckAt)
{
    LOCK(cs);
    CCouncilor* pmn = Find(outpoint);
    return pmn ? pmn->IsPingedWithin(nSeconds, nTimeToCheckAt) : false;
}

void CCouncilorMan::SetCouncilorLastPing(const COutPoint& outpoint, const CCouncilorPing& mnp)
{
    LOCK(cs);
    CCouncilor* pmn = Find(outpoint);
    if(!pmn) {
        return;
    }
    pmn->lastPing = mnp;
    // if councilor uses sentinel ping instead of watchdog
    // we shoud update nTimeLastWatchdogVote here if sentinel
    // ping flag is actual
    if(mnp.fSentinelIsCurrent) {
        UpdateWatchdogVoteTime(mnp.vin.prevout, mnp.sigTime);
    }
    mapSeenCouncilorPing.insert(std::make_pair(mnp.GetHash(), mnp));

    CCouncilorBroadcast mnb(*pmn);
    uint256 hash = mnb.GetHash();
    if(mapSeenCouncilorBroadcast.count(hash)) {
        mapSeenCouncilorBroadcast[hash].second.lastPing = mnp;
    }
}

void CCouncilorMan::UpdatedBlockTip(const CBlockIndex *pindex)
{
    nCachedBlockHeight = pindex->nHeight;
    LogPrint("councilor", "CCouncilorMan::UpdatedBlockTip -- nCachedBlockHeight=%d\n", nCachedBlockHeight);

    CheckSameAddr();

    if(fCounciLor) {
        // normal wallet does not need to update this every block, doing update on rpc call should be enough
        UpdateLastPaid(pindex);
    }
}

void CCouncilorMan::NotifyCouncilorUpdates(CConnman& connman)
{
    // Avoid double locking
    bool fCouncilorsAddedLocal = false;
    bool fCouncilorsRemovedLocal = false;
    {
        LOCK(cs);
        fCouncilorsAddedLocal = fCouncilorsAdded;
        fCouncilorsRemovedLocal = fCouncilorsRemoved;
    }

    if(fCouncilorsAddedLocal) {
        councilofricks.CheckCouncilorOrphanObjects(connman);
        councilofricks.CheckCouncilorOrphanVotes(connman);
    }
    if(fCouncilorsRemovedLocal) {
        councilofricks.UpdateCachesAndClean();
    }

    LOCK(cs);
    fCouncilorsAdded = false;
    fCouncilorsRemoved = false;
}
