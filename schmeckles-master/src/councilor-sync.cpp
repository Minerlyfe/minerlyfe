// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "activecouncilor.h"
#include "checkpoints.h"
#include "councilofricks.h"
#include "validation.h"
#include "councilor.h"
#include "councilor-payments.h"
#include "councilor-sync.h"
#include "councilorman.h"
#include "netfulfilledman.h"
#include "spork.h"
#include "util.h"

class CCouncilorSync;
CCouncilorSync councilorSync;

void CCouncilorSync::Fail()
{
    nTimeLastFailure = GetTime();
    nRequestedCouncilorAssets = COUNCILOR_SYNC_FAILED;
}

void CCouncilorSync::Reset()
{
    nRequestedCouncilorAssets = COUNCILOR_SYNC_INITIAL;
    nRequestedCouncilorAttempt = 0;
    nTimeAssetSyncStarted = GetTime();
    nTimeLastBumped = GetTime();
    nTimeLastFailure = 0;
}

void CCouncilorSync::BumpAssetLastTime(std::string strFuncName)
{
    if(IsSynced() || IsFailed()) return;
    nTimeLastBumped = GetTime();
    LogPrint("mnsync", "CCouncilorSync::BumpAssetLastTime -- %s\n", strFuncName);
}

std::string CCouncilorSync::GetAssetName()
{
    switch(nRequestedCouncilorAssets)
    {
        case(COUNCILOR_SYNC_INITIAL):      return "COUNCILOR_SYNC_INITIAL";
        case(COUNCILOR_SYNC_WAITING):      return "COUNCILOR_SYNC_WAITING";
        case(COUNCILOR_SYNC_LIST):         return "COUNCILOR_SYNC_LIST";
        case(COUNCILOR_SYNC_MNW):          return "COUNCILOR_SYNC_MNW";
        case(COUNCILOR_SYNC_COUNCILOFRICKS):   return "COUNCILOR_SYNC_COUNCILOFRICKS";
        case(COUNCILOR_SYNC_FAILED):       return "COUNCILOR_SYNC_FAILED";
        case COUNCILOR_SYNC_FINISHED:      return "COUNCILOR_SYNC_FINISHED";
        default:                            return "UNKNOWN";
    }
}

void CCouncilorSync::SwitchToNextAsset(CConnman& connman)
{
    switch(nRequestedCouncilorAssets)
    {
        case(COUNCILOR_SYNC_FAILED):
            throw std::runtime_error("Can't switch to next asset from failed, should use Reset() first!");
            break;
        case(COUNCILOR_SYNC_INITIAL):
            ClearFulfilledRequests(connman);
            nRequestedCouncilorAssets = COUNCILOR_SYNC_WAITING;
            LogPrintf("CCouncilorSync::SwitchToNextAsset -- Starting %s\n", GetAssetName());
            break;
        case(COUNCILOR_SYNC_WAITING):
            ClearFulfilledRequests(connman);
            LogPrintf("CCouncilorSync::SwitchToNextAsset -- Completed %s in %llds\n", GetAssetName(), GetTime() - nTimeAssetSyncStarted);
            nRequestedCouncilorAssets = COUNCILOR_SYNC_LIST;
            LogPrintf("CCouncilorSync::SwitchToNextAsset -- Starting %s\n", GetAssetName());
            break;
        case(COUNCILOR_SYNC_LIST):
            LogPrintf("CCouncilorSync::SwitchToNextAsset -- Completed %s in %llds\n", GetAssetName(), GetTime() - nTimeAssetSyncStarted);
            nRequestedCouncilorAssets = COUNCILOR_SYNC_MNW;
            LogPrintf("CCouncilorSync::SwitchToNextAsset -- Starting %s\n", GetAssetName());
            break;
        case(COUNCILOR_SYNC_MNW):
            LogPrintf("CCouncilorSync::SwitchToNextAsset -- Completed %s in %llds\n", GetAssetName(), GetTime() - nTimeAssetSyncStarted);
            nRequestedCouncilorAssets = COUNCILOR_SYNC_COUNCILOFRICKS;
            LogPrintf("CCouncilorSync::SwitchToNextAsset -- Starting %s\n", GetAssetName());
            break;
        case(COUNCILOR_SYNC_COUNCILOFRICKS):
            LogPrintf("CCouncilorSync::SwitchToNextAsset -- Completed %s in %llds\n", GetAssetName(), GetTime() - nTimeAssetSyncStarted);
            nRequestedCouncilorAssets = COUNCILOR_SYNC_FINISHED;
            uiInterface.NotifyAdditionalDataSyncProgressChanged(1);
            //try to activate our councilor if possible
            activeCouncilor.ManageState(connman);

            // TODO: Find out whether we can just use LOCK instead of:
            // TRY_LOCK(cs_vNodes, lockRecv);
            // if(lockRecv) { ... }

            connman.ForEachNode(CConnman::AllNodes, [](CNode* pnode) {
                netfulfilledman.AddFulfilledRequest(pnode->addr, "full-sync");
            });
            LogPrintf("CCouncilorSync::SwitchToNextAsset -- Sync has finished\n");

            break;
    }
    nRequestedCouncilorAttempt = 0;
    nTimeAssetSyncStarted = GetTime();
    BumpAssetLastTime("CCouncilorSync::SwitchToNextAsset");
}

std::string CCouncilorSync::GetSyncStatus()
{
    switch (councilorSync.nRequestedCouncilorAssets) {
        case COUNCILOR_SYNC_INITIAL:       return _("Synchroning blockchain...");
        case COUNCILOR_SYNC_WAITING:       return _("Synchronization pending...");
        case COUNCILOR_SYNC_LIST:          return _("Synchronizing councilors...");
        case COUNCILOR_SYNC_MNW:           return _("Synchronizing councilor payments...");
        case COUNCILOR_SYNC_COUNCILOFRICKS:    return _("Synchronizing councilofricks objects...");
        case COUNCILOR_SYNC_FAILED:        return _("Synchronization failed");
        case COUNCILOR_SYNC_FINISHED:      return _("Synchronization finished");
        default:                            return "";
    }
}

void CCouncilorSync::ProcessMessage(CNode* pfrom, std::string& strCommand, CDataStream& vRecv)
{
    if (strCommand == NetMsgType::SYNCSTATUSCOUNT) { //Sync status count

        //do not care about stats if sync process finished or failed
        if(IsSynced() || IsFailed()) return;

        int nItemID;
        int nCount;
        vRecv >> nItemID >> nCount;

        LogPrintf("SYNCSTATUSCOUNT -- got inventory count: nItemID=%d  nCount=%d  peer=%d\n", nItemID, nCount, pfrom->id);
    }
}

void CCouncilorSync::ClearFulfilledRequests(CConnman& connman)
{
    // TODO: Find out whether we can just use LOCK instead of:
    // TRY_LOCK(cs_vNodes, lockRecv);
    // if(!lockRecv) return;

    connman.ForEachNode(CConnman::AllNodes, [](CNode* pnode) {
        netfulfilledman.RemoveFulfilledRequest(pnode->addr, "spork-sync");
        netfulfilledman.RemoveFulfilledRequest(pnode->addr, "councilor-list-sync");
        netfulfilledman.RemoveFulfilledRequest(pnode->addr, "councilor-payment-sync");
        netfulfilledman.RemoveFulfilledRequest(pnode->addr, "councilofricks-sync");
        netfulfilledman.RemoveFulfilledRequest(pnode->addr, "full-sync");
    });
}

void CCouncilorSync::ProcessTick(CConnman& connman)
{
    static int nTick = 0;
    if(nTick++ % COUNCILOR_SYNC_TICK_SECONDS != 0) return;

    // reset the sync process if the last call to this function was more than 60 minutes ago (client was in sleep mode)
    static int64_t nTimeLastProcess = GetTime();
    if(GetTime() - nTimeLastProcess > 60*60) {
        LogPrintf("CCouncilorSync::HasSyncFailures -- WARNING: no actions for too long, restarting sync...\n");
        Reset();
        SwitchToNextAsset(connman);
        nTimeLastProcess = GetTime();
        return;
    }
    nTimeLastProcess = GetTime();

    // reset sync status in case of any other sync failure
    if(IsFailed()) {
        if(nTimeLastFailure + (1*60) < GetTime()) { // 1 minute cooldown after failed sync
            LogPrintf("CCouncilorSync::HasSyncFailures -- WARNING: failed to sync, trying again...\n");
            Reset();
            SwitchToNextAsset(connman);
        }
        return;
    }

    // gradually request the rest of the votes after sync finished
    if(IsSynced()) {
        std::vector<CNode*> vNodesCopy = connman.CopyNodeVector();
        councilofricks.RequestCouncilofRicksObjectVotes(vNodesCopy, connman);
        connman.ReleaseNodeVector(vNodesCopy);
        return;
    }

    // Calculate "progress" for LOG reporting / GUI notification
    double nSyncProgress = double(nRequestedCouncilorAttempt + (nRequestedCouncilorAssets - 1) * 8) / (8*4);
    LogPrintf("CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d nRequestedCouncilorAttempt %d nSyncProgress %f\n", nTick, nRequestedCouncilorAssets, nRequestedCouncilorAttempt, nSyncProgress);
    uiInterface.NotifyAdditionalDataSyncProgressChanged(nSyncProgress);

    std::vector<CNode*> vNodesCopy = connman.CopyNodeVector();

    BOOST_FOREACH(CNode* pnode, vNodesCopy)
    {
        // Don't try to sync any data from outbound "councilor" connections -
        // they are temporary and should be considered unreliable for a sync process.
        // Inbound connection this early is most likely a "councilor" connection
        // initiated from another node, so skip it too.
        if(pnode->fCouncilor || (fCounciLor && pnode->fInbound)) continue;

        // QUICK MODE (REGTEST ONLY!)
        if(Params().NetworkIDString() == CBaseChainParams::REGTEST)
        {
            if(nRequestedCouncilorAttempt <= 2) {
                connman.PushMessageWithVersion(pnode, INIT_PROTO_VERSION, NetMsgType::GETSPORKS); //get current network sporks
            } else if(nRequestedCouncilorAttempt < 4) {
                mnodeman.DsegUpdate(pnode, connman);
            } else if(nRequestedCouncilorAttempt < 6) {
                int nMnCount = mnodeman.CountCouncilors();
                connman.PushMessage(pnode, NetMsgType::COUNCILORPAYMENTSYNC, nMnCount); //sync payment votes
                SendCouncilofRicksSyncRequest(pnode, connman);
            } else {
                nRequestedCouncilorAssets = COUNCILOR_SYNC_FINISHED;
            }
            nRequestedCouncilorAttempt++;
            connman.ReleaseNodeVector(vNodesCopy);
            return;
        }

        // NORMAL NETWORK MODE - TESTNET/MAINNET
        {
            if(netfulfilledman.HasFulfilledRequest(pnode->addr, "full-sync")) {
                // We already fully synced from this node recently,
                // disconnect to free this connection slot for another peer.
                pnode->fDisconnect = true;
                LogPrintf("CCouncilorSync::ProcessTick -- disconnecting from recently synced peer %d\n", pnode->id);
                continue;
            }

            // SPORK : ALWAYS ASK FOR SPORKS AS WE SYNC

            if(!netfulfilledman.HasFulfilledRequest(pnode->addr, "spork-sync")) {
                // always get sporks first, only request once from each peer
                netfulfilledman.AddFulfilledRequest(pnode->addr, "spork-sync");
                // get current network sporks
                connman.PushMessageWithVersion(pnode, INIT_PROTO_VERSION, NetMsgType::GETSPORKS);
                LogPrintf("CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d -- requesting sporks from peer %d\n", nTick, nRequestedCouncilorAssets, pnode->id);
            }

            // INITIAL TIMEOUT

            if(nRequestedCouncilorAssets == COUNCILOR_SYNC_WAITING) {
                if(GetTime() - nTimeLastBumped > COUNCILOR_SYNC_TIMEOUT_SECONDS) {
                    // At this point we know that:
                    // a) there are peers (because we are looping on at least one of them);
                    // b) we waited for at least COUNCILOR_SYNC_TIMEOUT_SECONDS since we reached
                    //    the headers tip the last time (i.e. since we switched from
                    //     COUNCILOR_SYNC_INITIAL to COUNCILOR_SYNC_WAITING and bumped time);
                    // c) there were no blocks (UpdatedBlockTip, NotifyHeaderTip) or headers (AcceptedBlockHeader)
                    //    for at least COUNCILOR_SYNC_TIMEOUT_SECONDS.
                    // We must be at the tip already, let's move to the next asset.
                    SwitchToNextAsset(connman);
                }
            }

            // MNLIST : SYNC COUNCILOR LIST FROM OTHER CONNECTED CLIENTS

            if(nRequestedCouncilorAssets == COUNCILOR_SYNC_LIST) {
                LogPrint("councilor", "CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d nTimeLastBumped %lld GetTime() %lld diff %lld\n", nTick, nRequestedCouncilorAssets, nTimeLastBumped, GetTime(), GetTime() - nTimeLastBumped);
                // check for timeout first
                if(GetTime() - nTimeLastBumped > COUNCILOR_SYNC_TIMEOUT_SECONDS) {
                    LogPrintf("CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d -- timeout\n", nTick, nRequestedCouncilorAssets);
                    if (nRequestedCouncilorAttempt == 0) {
                        LogPrintf("CCouncilorSync::ProcessTick -- ERROR: failed to sync %s\n", GetAssetName());
                        // there is no way we can continue without councilor list, fail here and try later
                        Fail();
                        connman.ReleaseNodeVector(vNodesCopy);
                        return;
                    }
                    SwitchToNextAsset(connman);
                    connman.ReleaseNodeVector(vNodesCopy);
                    return;
                }

                // only request once from each peer
                if(netfulfilledman.HasFulfilledRequest(pnode->addr, "councilor-list-sync")) continue;
                netfulfilledman.AddFulfilledRequest(pnode->addr, "councilor-list-sync");

                if (pnode->nVersion < mnpayments.GetMinCouncilorPaymentsProto()) continue;
                nRequestedCouncilorAttempt++;

                mnodeman.DsegUpdate(pnode, connman);

                connman.ReleaseNodeVector(vNodesCopy);
                return; //this will cause each peer to get one request each six seconds for the various assets we need
            }

            // MNW : SYNC COUNCILOR PAYMENT VOTES FROM OTHER CONNECTED CLIENTS

            if(nRequestedCouncilorAssets == COUNCILOR_SYNC_MNW) {
                LogPrint("mnpayments", "CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d nTimeLastBumped %lld GetTime() %lld diff %lld\n", nTick, nRequestedCouncilorAssets, nTimeLastBumped, GetTime(), GetTime() - nTimeLastBumped);
                // check for timeout first
                // This might take a lot longer than COUNCILOR_SYNC_TIMEOUT_SECONDS due to new blocks,
                // but that should be OK and it should timeout eventually.
                if(GetTime() - nTimeLastBumped > COUNCILOR_SYNC_TIMEOUT_SECONDS) {
                    LogPrintf("CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d -- timeout\n", nTick, nRequestedCouncilorAssets);
                    if (nRequestedCouncilorAttempt == 0) {
                        LogPrintf("CCouncilorSync::ProcessTick -- ERROR: failed to sync %s\n", GetAssetName());
                        // probably not a good idea to proceed without winner list
                        Fail();
                        connman.ReleaseNodeVector(vNodesCopy);
                        return;
                    }
                    SwitchToNextAsset(connman);
                    connman.ReleaseNodeVector(vNodesCopy);
                    return;
                }

                // check for data
                // if mnpayments already has enough blocks and votes, switch to the next asset
                // try to fetch data from at least two peers though
                if(nRequestedCouncilorAttempt > 1 && mnpayments.IsEnoughData()) {
                    LogPrintf("CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d -- found enough data\n", nTick, nRequestedCouncilorAssets);
                    SwitchToNextAsset(connman);
                    connman.ReleaseNodeVector(vNodesCopy);
                    return;
                }

                // only request once from each peer
                if(netfulfilledman.HasFulfilledRequest(pnode->addr, "councilor-payment-sync")) continue;
                netfulfilledman.AddFulfilledRequest(pnode->addr, "councilor-payment-sync");

                if(pnode->nVersion < mnpayments.GetMinCouncilorPaymentsProto()) continue;
                nRequestedCouncilorAttempt++;

                // ask node for all payment votes it has (new nodes will only return votes for future payments)
                connman.PushMessage(pnode, NetMsgType::COUNCILORPAYMENTSYNC, mnpayments.GetStorageLimit());
                // ask node for missing pieces only (old nodes will not be asked)
                mnpayments.RequestLowDataPaymentBlocks(pnode, connman);

                connman.ReleaseNodeVector(vNodesCopy);
                return; //this will cause each peer to get one request each six seconds for the various assets we need
            }

            // GOVOBJ : SYNC COUNCILOFRICKS ITEMS FROM OUR PEERS

            if(nRequestedCouncilorAssets == COUNCILOR_SYNC_COUNCILOFRICKS) {
                LogPrint("gobject", "CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d nTimeLastBumped %lld GetTime() %lld diff %lld\n", nTick, nRequestedCouncilorAssets, nTimeLastBumped, GetTime(), GetTime() - nTimeLastBumped);

                // check for timeout first
                if(GetTime() - nTimeLastBumped > COUNCILOR_SYNC_TIMEOUT_SECONDS) {
                    LogPrintf("CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d -- timeout\n", nTick, nRequestedCouncilorAssets);
                    if(nRequestedCouncilorAttempt == 0) {
                        LogPrintf("CCouncilorSync::ProcessTick -- WARNING: failed to sync %s\n", GetAssetName());
                        // it's kind of ok to skip this for now, hopefully we'll catch up later?
                    }
                    SwitchToNextAsset(connman);
                    connman.ReleaseNodeVector(vNodesCopy);
                    return;
                }

                // only request obj sync once from each peer, then request votes on per-obj basis
                if(netfulfilledman.HasFulfilledRequest(pnode->addr, "councilofricks-sync")) {
                    int nObjsLeftToAsk = councilofricks.RequestCouncilofRicksObjectVotes(pnode, connman);
                    static int64_t nTimeNoObjectsLeft = 0;
                    // check for data
                    if(nObjsLeftToAsk == 0) {
                        static int nLastTick = 0;
                        static int nLastVotes = 0;
                        if(nTimeNoObjectsLeft == 0) {
                            // asked all objects for votes for the first time
                            nTimeNoObjectsLeft = GetTime();
                        }
                        // make sure the condition below is checked only once per tick
                        if(nLastTick == nTick) continue;
                        if(GetTime() - nTimeNoObjectsLeft > COUNCILOR_SYNC_TIMEOUT_SECONDS &&
                            councilofricks.GetVoteCount() - nLastVotes < std::max(int(0.0001 * nLastVotes), COUNCILOR_SYNC_TICK_SECONDS)
                        ) {
                            // We already asked for all objects, waited for COUNCILOR_SYNC_TIMEOUT_SECONDS
                            // after that and less then 0.01% or COUNCILOR_SYNC_TICK_SECONDS
                            // (i.e. 1 per second) votes were recieved during the last tick.
                            // We can be pretty sure that we are done syncing.
                            LogPrintf("CCouncilorSync::ProcessTick -- nTick %d nRequestedCouncilorAssets %d -- asked for all objects, nothing to do\n", nTick, nRequestedCouncilorAssets);
                            // reset nTimeNoObjectsLeft to be able to use the same condition on resync
                            nTimeNoObjectsLeft = 0;
                            SwitchToNextAsset(connman);
                            connman.ReleaseNodeVector(vNodesCopy);
                            return;
                        }
                        nLastTick = nTick;
                        nLastVotes = councilofricks.GetVoteCount();
                    }
                    continue;
                }
                netfulfilledman.AddFulfilledRequest(pnode->addr, "councilofricks-sync");

                if (pnode->nVersion < MIN_COUNCILOFRICKS_PEER_PROTO_VERSION) continue;
                nRequestedCouncilorAttempt++;

                SendCouncilofRicksSyncRequest(pnode, connman);

                connman.ReleaseNodeVector(vNodesCopy);
                return; //this will cause each peer to get one request each six seconds for the various assets we need
            }
        }
    }
    // looped through all nodes, release them
    connman.ReleaseNodeVector(vNodesCopy);
}

void CCouncilorSync::SendCouncilofRicksSyncRequest(CNode* pnode, CConnman& connman)
{
    if(pnode->nVersion >= COUNCILOFRICKS_FILTER_PROTO_VERSION) {
        CBloomFilter filter;
        filter.clear();

        connman.PushMessage(pnode, NetMsgType::MNCOUNCILOFRICKSSYNC, uint256(), filter);
    }
    else {
        connman.PushMessage(pnode, NetMsgType::MNCOUNCILOFRICKSSYNC, uint256());
    }
}

void CCouncilorSync::AcceptedBlockHeader(const CBlockIndex *pindexNew)
{
    LogPrint("mnsync", "CCouncilorSync::AcceptedBlockHeader -- pindexNew->nHeight: %d\n", pindexNew->nHeight);

    if (!IsBlockchainSynced()) {
        // Postpone timeout each time new block header arrives while we are still syncing blockchain
        BumpAssetLastTime("CCouncilorSync::AcceptedBlockHeader");
    }
}

void CCouncilorSync::NotifyHeaderTip(const CBlockIndex *pindexNew, bool fInitialDownload, CConnman& connman)
{
    LogPrint("mnsync", "CCouncilorSync::NotifyHeaderTip -- pindexNew->nHeight: %d fInitialDownload=%d\n", pindexNew->nHeight, fInitialDownload);

    if (IsFailed() || IsSynced() || !pindexBestHeader)
        return;

    if (!IsBlockchainSynced()) {
        // Postpone timeout each time new block arrives while we are still syncing blockchain
        BumpAssetLastTime("CCouncilorSync::NotifyHeaderTip");
    }
}

void CCouncilorSync::UpdatedBlockTip(const CBlockIndex *pindexNew, bool fInitialDownload, CConnman& connman)
{
    LogPrint("mnsync", "CCouncilorSync::UpdatedBlockTip -- pindexNew->nHeight: %d fInitialDownload=%d\n", pindexNew->nHeight, fInitialDownload);

    if (IsFailed() || IsSynced() || !pindexBestHeader)
        return;

    if (!IsBlockchainSynced()) {
        // Postpone timeout each time new block arrives while we are still syncing blockchain
        BumpAssetLastTime("CCouncilorSync::UpdatedBlockTip");
    }

    if (fInitialDownload) {
        // switched too early
        if (IsBlockchainSynced()) {
            Reset();
        }

        // no need to check any further while still in IBD mode
        return;
    }

    // Note: since we sync headers first, it should be ok to use this
    static bool fReachedBestHeader = false;
    bool fReachedBestHeaderNew = pindexNew->GetBlockHash() == pindexBestHeader->GetBlockHash();

    if (fReachedBestHeader && !fReachedBestHeaderNew) {
        // Switching from true to false means that we previousely stuck syncing headers for some reason,
        // probably initial timeout was not enough,
        // because there is no way we can update tip not having best header
        Reset();
        fReachedBestHeader = false;
        return;
    }

    fReachedBestHeader = fReachedBestHeaderNew;

    LogPrint("mnsync", "CCouncilorSync::UpdatedBlockTip -- pindexNew->nHeight: %d pindexBestHeader->nHeight: %d fInitialDownload=%d fReachedBestHeader=%d\n",
                pindexNew->nHeight, pindexBestHeader->nHeight, fInitialDownload, fReachedBestHeader);

    if (!IsBlockchainSynced() && fReachedBestHeader) {
        // Reached best header while being in initial mode.
        // We must be at the tip already, let's move to the next asset.
        SwitchToNextAsset(connman);
    }
}
