#ifndef COUNCILORLIST_H
#define COUNCILORLIST_H

#include "primitives/transaction.h"
#include "platformstyle.h"
#include "sync.h"
#include "util.h"

#include <QMenu>
#include <QTimer>
#include <QWidget>

#define MY_COUNCILORLIST_UPDATE_SECONDS                 60
#define COUNCILORLIST_UPDATE_SECONDS                    15
#define COUNCILORLIST_FILTER_COOLDOWN_SECONDS            3

namespace Ui {
    class CouncilorList;
}

class ClientModel;
class WalletModel;

QT_BEGIN_NAMESPACE
class QModelIndex;
QT_END_NAMESPACE

/** Councilor Manager page widget */
class CouncilorList : public QWidget
{
    Q_OBJECT

public:
    explicit CouncilorList(const PlatformStyle *platformStyle, QWidget *parent = 0);
    ~CouncilorList();

    void setClientModel(ClientModel *clientModel);
    void setWalletModel(WalletModel *walletModel);
    void StartAlias(std::string strAlias);
    void StartAll(std::string strCommand = "start-all");

private:
    QMenu *contextMenu;
    int64_t nTimeFilterUpdated;
    bool fFilterUpdated;

public Q_SLOTS:
    void updateMyCouncilorInfo(QString strAlias, QString strAddr, const COutPoint& outpoint);
    void updateMyNodeList(bool fForce = false);
    void updateNodeList();

Q_SIGNALS:

private:
    QTimer *timer;
    Ui::CouncilorList *ui;
    ClientModel *clientModel;
    WalletModel *walletModel;

    // Protects tableWidgetCouncilors
    CCriticalSection cs_mnlist;

    // Protects tableWidgetMyCouncilors
    CCriticalSection cs_mymnlist;

    QString strCurrentFilter;

private Q_SLOTS:
    void showContextMenu(const QPoint &);
    void on_filterLineEdit_textChanged(const QString &strFilterIn);
    void on_startButton_clicked();
    void on_startAllButton_clicked();
    void on_startMissingButton_clicked();
    void on_tableWidgetMyCouncilors_itemSelectionChanged();
    void on_UpdateButton_clicked();
};
#endif // COUNCILORLIST_H
