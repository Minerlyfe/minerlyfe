#include "councilorlist.h"
#include "ui_councilorlist.h"

#include "activecouncilor.h"
#include "clientmodel.h"
#include "init.h"
#include "guiutil.h"
#include "councilor-sync.h"
#include "councilorconfig.h"
#include "councilorman.h"
#include "sync.h"
#include "wallet/wallet.h"
#include "walletmodel.h"

#include <QTimer>
#include <QMessageBox>

int GetOffsetFromUtc()
{
#if QT_VERSION < 0x050200
    const QDateTime dateTime1 = QDateTime::currentDateTime();
    const QDateTime dateTime2 = QDateTime(dateTime1.date(), dateTime1.time(), Qt::UTC);
    return dateTime1.secsTo(dateTime2);
#else
    return QDateTime::currentDateTime().offsetFromUtc();
#endif
}

CouncilorList::CouncilorList(const PlatformStyle *platformStyle, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CouncilorList),
    clientModel(0),
    walletModel(0)
{
    ui->setupUi(this);

    ui->startButton->setEnabled(false);

    int columnAliasWidth = 100;
    int columnAddressWidth = 200;
    int columnProtocolWidth = 60;
    int columnStatusWidth = 80;
    int columnActiveWidth = 130;
    int columnLastSeenWidth = 130;

    ui->tableWidgetMyCouncilors->setColumnWidth(0, columnAliasWidth);
    ui->tableWidgetMyCouncilors->setColumnWidth(1, columnAddressWidth);
    ui->tableWidgetMyCouncilors->setColumnWidth(2, columnProtocolWidth);
    ui->tableWidgetMyCouncilors->setColumnWidth(3, columnStatusWidth);
    ui->tableWidgetMyCouncilors->setColumnWidth(4, columnActiveWidth);
    ui->tableWidgetMyCouncilors->setColumnWidth(5, columnLastSeenWidth);

    ui->tableWidgetCouncilors->setColumnWidth(0, columnAddressWidth);
    ui->tableWidgetCouncilors->setColumnWidth(1, columnProtocolWidth);
    ui->tableWidgetCouncilors->setColumnWidth(2, columnStatusWidth);
    ui->tableWidgetCouncilors->setColumnWidth(3, columnActiveWidth);
    ui->tableWidgetCouncilors->setColumnWidth(4, columnLastSeenWidth);

    ui->tableWidgetMyCouncilors->setContextMenuPolicy(Qt::CustomContextMenu);

    QAction *startAliasAction = new QAction(tr("Start alias"), this);
    contextMenu = new QMenu();
    contextMenu->addAction(startAliasAction);
    connect(ui->tableWidgetMyCouncilors, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(showContextMenu(const QPoint&)));
    connect(startAliasAction, SIGNAL(triggered()), this, SLOT(on_startButton_clicked()));

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateNodeList()));
    connect(timer, SIGNAL(timeout()), this, SLOT(updateMyNodeList()));
    timer->start(1000);

    fFilterUpdated = false;
    nTimeFilterUpdated = GetTime();
    updateNodeList();
}

CouncilorList::~CouncilorList()
{
    delete ui;
}

void CouncilorList::setClientModel(ClientModel *model)
{
    this->clientModel = model;
    if(model) {
        // try to update list when councilor count changes
        connect(clientModel, SIGNAL(strCouncilorsChanged(QString)), this, SLOT(updateNodeList()));
    }
}

void CouncilorList::setWalletModel(WalletModel *model)
{
    this->walletModel = model;
}

void CouncilorList::showContextMenu(const QPoint &point)
{
    QTableWidgetItem *item = ui->tableWidgetMyCouncilors->itemAt(point);
    if(item) contextMenu->exec(QCursor::pos());
}

void CouncilorList::StartAlias(std::string strAlias)
{
    std::string strStatusHtml;
    strStatusHtml += "<center>Alias: " + strAlias;

    BOOST_FOREACH(CCouncilorConfig::CCouncilorEntry mne, councilorConfig.getEntries()) {
        if(mne.getAlias() == strAlias) {
            std::string strError;
            CCouncilorBroadcast mnb;

            bool fSuccess = CCouncilorBroadcast::Create(mne.getIp(), mne.getPrivKey(), mne.getTxHash(), mne.getOutputIndex(), strError, mnb);

            if(fSuccess) {
                strStatusHtml += "<br>Successfully started councilor.";
                mnodeman.UpdateCouncilorList(mnb, *g_connman);
                mnb.Relay(*g_connman);
                mnodeman.NotifyCouncilorUpdates(*g_connman);
            } else {
                strStatusHtml += "<br>Failed to start councilor.<br>Error: " + strError;
            }
            break;
        }
    }
    strStatusHtml += "</center>";

    QMessageBox msg;
    msg.setText(QString::fromStdString(strStatusHtml));
    msg.exec();

    updateMyNodeList(true);
}

void CouncilorList::StartAll(std::string strCommand)
{
    int nCountSuccessful = 0;
    int nCountFailed = 0;
    std::string strFailedHtml;

    BOOST_FOREACH(CCouncilorConfig::CCouncilorEntry mne, councilorConfig.getEntries()) {
        std::string strError;
        CCouncilorBroadcast mnb;

        int32_t nOutputIndex = 0;
        if(!ParseInt32(mne.getOutputIndex(), &nOutputIndex)) {
            continue;
        }

        COutPoint outpoint = COutPoint(uint256S(mne.getTxHash()), nOutputIndex);

        if(strCommand == "start-missing" && mnodeman.Has(outpoint)) continue;

        bool fSuccess = CCouncilorBroadcast::Create(mne.getIp(), mne.getPrivKey(), mne.getTxHash(), mne.getOutputIndex(), strError, mnb);

        if(fSuccess) {
            nCountSuccessful++;
            mnodeman.UpdateCouncilorList(mnb, *g_connman);
            mnb.Relay(*g_connman);
            mnodeman.NotifyCouncilorUpdates(*g_connman);
        } else {
            nCountFailed++;
            strFailedHtml += "\nFailed to start " + mne.getAlias() + ". Error: " + strError;
        }
    }
    pwalletMain->Lock();

    std::string returnObj;
    returnObj = strprintf("Successfully started %d councilors, failed to start %d, total %d", nCountSuccessful, nCountFailed, nCountFailed + nCountSuccessful);
    if (nCountFailed > 0) {
        returnObj += strFailedHtml;
    }

    QMessageBox msg;
    msg.setText(QString::fromStdString(returnObj));
    msg.exec();

    updateMyNodeList(true);
}

void CouncilorList::updateMyCouncilorInfo(QString strAlias, QString strAddr, const COutPoint& outpoint)
{
    bool fOldRowFound = false;
    int nNewRow = 0;

    for(int i = 0; i < ui->tableWidgetMyCouncilors->rowCount(); i++) {
        if(ui->tableWidgetMyCouncilors->item(i, 0)->text() == strAlias) {
            fOldRowFound = true;
            nNewRow = i;
            break;
        }
    }

    if(nNewRow == 0 && !fOldRowFound) {
        nNewRow = ui->tableWidgetMyCouncilors->rowCount();
        ui->tableWidgetMyCouncilors->insertRow(nNewRow);
    }

    councilor_info_t infoMn;
    bool fFound = mnodeman.GetCouncilorInfo(outpoint, infoMn);

    QTableWidgetItem *aliasItem = new QTableWidgetItem(strAlias);
    QTableWidgetItem *addrItem = new QTableWidgetItem(fFound ? QString::fromStdString(infoMn.addr.ToString()) : strAddr);
    QTableWidgetItem *protocolItem = new QTableWidgetItem(QString::number(fFound ? infoMn.nProtocolVersion : -1));
    QTableWidgetItem *statusItem = new QTableWidgetItem(QString::fromStdString(fFound ? CCouncilor::StateToString(infoMn.nActiveState) : "MISSING"));
    QTableWidgetItem *activeSecondsItem = new QTableWidgetItem(QString::fromStdString(DurationToDHMS(fFound ? (infoMn.nTimeLastPing - infoMn.sigTime) : 0)));
    QTableWidgetItem *lastSeenItem = new QTableWidgetItem(QString::fromStdString(DateTimeStrFormat("%Y-%m-%d %H:%M",
                                                                                                   fFound ? infoMn.nTimeLastPing + GetOffsetFromUtc() : 0)));
    QTableWidgetItem *pubkeyItem = new QTableWidgetItem(QString::fromStdString(fFound ? CBitcoinAddress(infoMn.pubKeyCollateralAddress.GetID()).ToString() : ""));

    ui->tableWidgetMyCouncilors->setItem(nNewRow, 0, aliasItem);
    ui->tableWidgetMyCouncilors->setItem(nNewRow, 1, addrItem);
    ui->tableWidgetMyCouncilors->setItem(nNewRow, 2, protocolItem);
    ui->tableWidgetMyCouncilors->setItem(nNewRow, 3, statusItem);
    ui->tableWidgetMyCouncilors->setItem(nNewRow, 4, activeSecondsItem);
    ui->tableWidgetMyCouncilors->setItem(nNewRow, 5, lastSeenItem);
    ui->tableWidgetMyCouncilors->setItem(nNewRow, 6, pubkeyItem);
}

void CouncilorList::updateMyNodeList(bool fForce)
{
    TRY_LOCK(cs_mymnlist, fLockAcquired);
    if(!fLockAcquired) {
        return;
    }
    static int64_t nTimeMyListUpdated = 0;

    // automatically update my councilor list only once in MY_COUNCILORLIST_UPDATE_SECONDS seconds,
    // this update still can be triggered manually at any time via button click
    int64_t nSecondsTillUpdate = nTimeMyListUpdated + MY_COUNCILORLIST_UPDATE_SECONDS - GetTime();
    ui->secondsLabel->setText(QString::number(nSecondsTillUpdate));

    if(nSecondsTillUpdate > 0 && !fForce) return;
    nTimeMyListUpdated = GetTime();

    ui->tableWidgetCouncilors->setSortingEnabled(false);
    BOOST_FOREACH(CCouncilorConfig::CCouncilorEntry mne, councilorConfig.getEntries()) {
        int32_t nOutputIndex = 0;
        if(!ParseInt32(mne.getOutputIndex(), &nOutputIndex)) {
            continue;
        }

        updateMyCouncilorInfo(QString::fromStdString(mne.getAlias()), QString::fromStdString(mne.getIp()), COutPoint(uint256S(mne.getTxHash()), nOutputIndex));
    }
    ui->tableWidgetCouncilors->setSortingEnabled(true);

    // reset "timer"
    ui->secondsLabel->setText("0");
}

void CouncilorList::updateNodeList()
{
    TRY_LOCK(cs_mnlist, fLockAcquired);
    if(!fLockAcquired) {
        return;
    }

    static int64_t nTimeListUpdated = GetTime();

    // to prevent high cpu usage update only once in COUNCILORLIST_UPDATE_SECONDS seconds
    // or COUNCILORLIST_FILTER_COOLDOWN_SECONDS seconds after filter was last changed
    int64_t nSecondsToWait = fFilterUpdated
                            ? nTimeFilterUpdated - GetTime() + COUNCILORLIST_FILTER_COOLDOWN_SECONDS
                            : nTimeListUpdated - GetTime() + COUNCILORLIST_UPDATE_SECONDS;

    if(fFilterUpdated) ui->countLabel->setText(QString::fromStdString(strprintf("Please wait... %d", nSecondsToWait)));
    if(nSecondsToWait > 0) return;

    nTimeListUpdated = GetTime();
    fFilterUpdated = false;

    QString strToFilter;
    ui->countLabel->setText("Updating...");
    ui->tableWidgetCouncilors->setSortingEnabled(false);
    ui->tableWidgetCouncilors->clearContents();
    ui->tableWidgetCouncilors->setRowCount(0);
    std::map<COutPoint, CCouncilor> mapCouncilors = mnodeman.GetFullCouncilorMap();
    int offsetFromUtc = GetOffsetFromUtc();

    for(auto& mnpair : mapCouncilors)
    {
        CCouncilor mn = mnpair.second;
        // populate list
        // Address, Protocol, Status, Active Seconds, Last Seen, Pub Key
        QTableWidgetItem *addressItem = new QTableWidgetItem(QString::fromStdString(mn.addr.ToString()));
        QTableWidgetItem *protocolItem = new QTableWidgetItem(QString::number(mn.nProtocolVersion));
        QTableWidgetItem *statusItem = new QTableWidgetItem(QString::fromStdString(mn.GetStatus()));
        QTableWidgetItem *activeSecondsItem = new QTableWidgetItem(QString::fromStdString(DurationToDHMS(mn.lastPing.sigTime - mn.sigTime)));
        QTableWidgetItem *lastSeenItem = new QTableWidgetItem(QString::fromStdString(DateTimeStrFormat("%Y-%m-%d %H:%M", mn.lastPing.sigTime + offsetFromUtc)));
        QTableWidgetItem *pubkeyItem = new QTableWidgetItem(QString::fromStdString(CBitcoinAddress(mn.pubKeyCollateralAddress.GetID()).ToString()));

        if (strCurrentFilter != "")
        {
            strToFilter =   addressItem->text() + " " +
                            protocolItem->text() + " " +
                            statusItem->text() + " " +
                            activeSecondsItem->text() + " " +
                            lastSeenItem->text() + " " +
                            pubkeyItem->text();
            if (!strToFilter.contains(strCurrentFilter)) continue;
        }

        ui->tableWidgetCouncilors->insertRow(0);
        ui->tableWidgetCouncilors->setItem(0, 0, addressItem);
        ui->tableWidgetCouncilors->setItem(0, 1, protocolItem);
        ui->tableWidgetCouncilors->setItem(0, 2, statusItem);
        ui->tableWidgetCouncilors->setItem(0, 3, activeSecondsItem);
        ui->tableWidgetCouncilors->setItem(0, 4, lastSeenItem);
        ui->tableWidgetCouncilors->setItem(0, 5, pubkeyItem);
    }

    ui->countLabel->setText(QString::number(ui->tableWidgetCouncilors->rowCount()));
    ui->tableWidgetCouncilors->setSortingEnabled(true);
}

void CouncilorList::on_filterLineEdit_textChanged(const QString &strFilterIn)
{
    strCurrentFilter = strFilterIn;
    nTimeFilterUpdated = GetTime();
    fFilterUpdated = true;
    ui->countLabel->setText(QString::fromStdString(strprintf("Please wait... %d", COUNCILORLIST_FILTER_COOLDOWN_SECONDS)));
}

void CouncilorList::on_startButton_clicked()
{
    std::string strAlias;
    {
        LOCK(cs_mymnlist);
        // Find selected node alias
        QItemSelectionModel* selectionModel = ui->tableWidgetMyCouncilors->selectionModel();
        QModelIndexList selected = selectionModel->selectedRows();

        if(selected.count() == 0) return;

        QModelIndex index = selected.at(0);
        int nSelectedRow = index.row();
        strAlias = ui->tableWidgetMyCouncilors->item(nSelectedRow, 0)->text().toStdString();
    }

    // Display message box
    QMessageBox::StandardButton retval = QMessageBox::question(this, tr("Confirm councilor start"),
        tr("Are you sure you want to start councilor %1?").arg(QString::fromStdString(strAlias)),
        QMessageBox::Yes | QMessageBox::Cancel,
        QMessageBox::Cancel);

    if(retval != QMessageBox::Yes) return;

    WalletModel::EncryptionStatus encStatus = walletModel->getEncryptionStatus();

    if(encStatus == walletModel->Locked || encStatus == walletModel->UnlockedForMixingOnly) {
        WalletModel::UnlockContext ctx(walletModel->requestUnlock());

        if(!ctx.isValid()) return; // Unlock wallet was cancelled

        StartAlias(strAlias);
        return;
    }

    StartAlias(strAlias);
}

void CouncilorList::on_startAllButton_clicked()
{
    // Display message box
    QMessageBox::StandardButton retval = QMessageBox::question(this, tr("Confirm all councilors start"),
        tr("Are you sure you want to start ALL councilors?"),
        QMessageBox::Yes | QMessageBox::Cancel,
        QMessageBox::Cancel);

    if(retval != QMessageBox::Yes) return;

    WalletModel::EncryptionStatus encStatus = walletModel->getEncryptionStatus();

    if(encStatus == walletModel->Locked || encStatus == walletModel->UnlockedForMixingOnly) {
        WalletModel::UnlockContext ctx(walletModel->requestUnlock());

        if(!ctx.isValid()) return; // Unlock wallet was cancelled

        StartAll();
        return;
    }

    StartAll();
}

void CouncilorList::on_startMissingButton_clicked()
{

    if(!councilorSync.IsCouncilorListSynced()) {
        QMessageBox::critical(this, tr("Command is not available right now"),
            tr("You can't use this command until councilor list is synced"));
        return;
    }

    // Display message box
    QMessageBox::StandardButton retval = QMessageBox::question(this,
        tr("Confirm missing councilors start"),
        tr("Are you sure you want to start MISSING councilors?"),
        QMessageBox::Yes | QMessageBox::Cancel,
        QMessageBox::Cancel);

    if(retval != QMessageBox::Yes) return;

    WalletModel::EncryptionStatus encStatus = walletModel->getEncryptionStatus();

    if(encStatus == walletModel->Locked || encStatus == walletModel->UnlockedForMixingOnly) {
        WalletModel::UnlockContext ctx(walletModel->requestUnlock());

        if(!ctx.isValid()) return; // Unlock wallet was cancelled

        StartAll("start-missing");
        return;
    }

    StartAll("start-missing");
}

void CouncilorList::on_tableWidgetMyCouncilors_itemSelectionChanged()
{
    if(ui->tableWidgetMyCouncilors->selectedItems().count() > 0) {
        ui->startButton->setEnabled(true);
    }
}

void CouncilorList::on_UpdateButton_clicked()
{
    updateMyNodeList(true);
}
