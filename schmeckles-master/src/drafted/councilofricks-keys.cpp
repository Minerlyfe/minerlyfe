
// // Copyright (c) 2014-2017 The Dash Core developers
// // Distributed under the MIT/X11 software license, see the accompanying
// // file COPYING or http://www.opensource.org/licenses/mit-license.php.

// #ifndef COUNCILOFRICKS_KEYS_H
// #define COUNCILOFRICKS_KEYS_H

// #include <string>
// #include <vector>
// #include <map>

// #include <univalue.h>
// #include "support/allocators/secure.h"
// #include ""

// vector<CCouncilofRicksKey> vCouncilofRicksKeys;
// CCriticalSection cs_vCouncilofRicksKeys;

// bool CCouncilofRicksKeyManager::InitCouncilofRicksKeys(std::string strError)
// {

//     {
//         LOCK(cs_vCouncilofRicksKeys);
//         vCouncilofRicksKeys = mapMultiArgs["-addgovkey"];
//     }

//     BOOST_FOREACH(SecureString& strSecure, vCouncilofRicksKeys)
//     {
//     	std::vector<std::string> vecTokenized = SplitBy(strSubCommand, ":");

//     	if(vecTokenized.size() == 2) continue;

// 	    CBitcoinSecret vchSecret;
// 	    bool fGood = vchSecret.SetString(vecTokenized[0]);

// 	    if(!fGood) {
// 	    	strError = "Invalid CouncilofRicks Key : " + vecTokenized[0];
// 	    	return false;
// 	    }

//     	CCouncilofRicksKey key(vecTokenized[0], vecTokenized[1]);
//     	vCouncilofRicksKeys.push_back(key);
//     }
// }