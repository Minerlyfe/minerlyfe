
#include "councilofricks-types.h"

CouncilofRicksObjectType CouncilofRicksStringToType(std::string strType) {
    CouncilofRicksObjectType type = Error;

    if(strType == "AllTypes") type = AllTypes;
    if(strType == "Error") type = Error;
    if(strType == "ValueOverride") type = ValueOverride;
    if(strType == "SchmecklesNetwork") type = SchmecklesNetwork;
    if(strType == "SchmecklesNetworkVariable") type = SchmecklesNetworkVariable;
    if(strType == "Category") type = Category;
    if(strType == "Group") type = Group;
    if(strType == "User") type = User;
    if(strType == "Company") type = Company;
    if(strType == "Project") type = Project;
    if(strType == "ProjectReport") type = ProjectReport;
    if(strType == "ProjectMilestone") type = ProjectMilestone;
    if(strType == "Proposal") type = Proposal;
    if(strType == "Contract") type = Contract;

    return type;
};

std::string CouncilofRicksTypeToString(CouncilofRicksObjectType type) {
    std::string s = "";

    if(type == AllTypes) s = "AllTypes";
    if(type == Error) s = "Error";
    if(type == ValueOverride) s = "ValueOverride";
    if(type == SchmecklesNetwork) s = "SchmecklesNetwork";
    if(type == SchmecklesNetworkVariable) s = "SchmecklesNetworkVariable";
    if(type == Category) s = "Category";
    if(type == Group) s = "Group";
    if(type == User) s = "User";
    if(type == Company) s = "Company";
    if(type == Project) s = "Project";
    if(type == ProjectReport) s = "ProjectReport";
    if(type == ProjectMilestone) s = "ProjectMilestone";
    if(type == Proposal) s = "Proposal";
    if(type == Contract) s = "Contract";

    return s;
};