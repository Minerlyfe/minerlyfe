// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "activecouncilor.h"
#include "councilofricks-classes.h"
#include "councilor-payments.h"
#include "councilor-sync.h"
#include "councilorman.h"
#include "messagesigner.h"
#include "netfulfilledman.h"
#include "spork.h"
#include "util.h"

#include <boost/lexical_cast.hpp>

/** Object for who's going to get paid on which blocks */
CCouncilorPayments mnpayments;

CCriticalSection cs_vecPayees;
CCriticalSection cs_mapCouncilorBlocks;
CCriticalSection cs_mapCouncilorPaymentVotes;

/**
* IsBlockValueValid
*
*   Determine if coinbase outgoing created money is the correct value
*
*   Why is this needed?
*   - In Schmeckles some blocks are superblocks, which output much higher amounts of coins
*   - Otherblocks are 10% lower in outgoing value, so in total, no extra coins are created
*   - When non-superblocks are detected, the normal schedule should be maintained
*/

bool IsBlockValueValid(const CBlock& block, int nBlockHeight, CAmount blockReward, std::string &strErrorRet)
{
    strErrorRet = "";

    bool isBlockRewardValueMet = (block.vtx[0].GetValueOut() <= blockReward);
    if(fDebug) LogPrintf("block.vtx[0].GetValueOut() %lld <= blockReward %lld\n", block.vtx[0].GetValueOut(), blockReward);

    // we are still using budgets, but we have no data about them anymore,
    // all we know is predefined budget cycle and window

    const Consensus::Params& consensusParams = Params().GetConsensus();

    if(nBlockHeight < consensusParams.nSuperblockStartBlock) {
        int nOffset = nBlockHeight % consensusParams.nBudgetPaymentsCycleBlocks;
        if(nBlockHeight >= consensusParams.nBudgetPaymentsStartBlock &&
            nOffset < consensusParams.nBudgetPaymentsWindowBlocks) {
            // NOTE: make sure SPORK_13_OLD_SUPERBLOCK_FLAG is disabled when 12.1 starts to go live
            if(councilorSync.IsSynced() && !sporkManager.IsSporkActive(SPORK_13_OLD_SUPERBLOCK_FLAG)) {
                // no budget blocks should be accepted here, if SPORK_13_OLD_SUPERBLOCK_FLAG is disabled
                LogPrint("gobject", "IsBlockValueValid -- Client synced but budget spork is disabled, checking block value against block reward\n");
                if(!isBlockRewardValueMet) {
                    strErrorRet = strprintf("coinbase pays too much at height %d (actual=%d vs limit=%d), exceeded block reward, budgets are disabled",
                                            nBlockHeight, block.vtx[0].GetValueOut(), blockReward);
                }
                return isBlockRewardValueMet;
            }
            LogPrint("gobject", "IsBlockValueValid -- WARNING: Skipping budget block value checks, accepting block\n");
            // TODO: reprocess blocks to make sure they are legit?
            return true;
        }
        // LogPrint("gobject", "IsBlockValueValid -- Block is not in budget cycle window, checking block value against block reward\n");
        if(!isBlockRewardValueMet) {
            strErrorRet = strprintf("coinbase pays too much at height %d (actual=%d vs limit=%d), exceeded block reward, block is not in budget cycle window",
                                    nBlockHeight, block.vtx[0].GetValueOut(), blockReward);
        }
        return isBlockRewardValueMet;
    }

    // superblocks started

    CAmount nSuperblockMaxValue =  blockReward + CSuperblock::GetPaymentsLimit(nBlockHeight);
    bool isSuperblockMaxValueMet = (block.vtx[0].GetValueOut() <= nSuperblockMaxValue);

    LogPrint("gobject", "block.vtx[0].GetValueOut() %lld <= nSuperblockMaxValue %lld\n", block.vtx[0].GetValueOut(), nSuperblockMaxValue);

    if(!councilorSync.IsSynced()) {
        // not enough data but at least it must NOT exceed superblock max value
        if(CSuperblock::IsValidBlockHeight(nBlockHeight)) {
            if(fDebug) LogPrintf("IsBlockPayeeValid -- WARNING: Client not synced, checking superblock max bounds only\n");
            if(!isSuperblockMaxValueMet) {
                strErrorRet = strprintf("coinbase pays too much at height %d (actual=%d vs limit=%d), exceeded superblock max value",
                                        nBlockHeight, block.vtx[0].GetValueOut(), nSuperblockMaxValue);
            }
            return isSuperblockMaxValueMet;
        }
        if(!isBlockRewardValueMet) {
            strErrorRet = strprintf("coinbase pays too much at height %d (actual=%d vs limit=%d), exceeded block reward, only regular blocks are allowed at this height",
                                    nBlockHeight, block.vtx[0].GetValueOut(), blockReward);
        }
        // it MUST be a regular block otherwise
        return isBlockRewardValueMet;
    }

    // we are synced, let's try to check as much data as we can

    if(sporkManager.IsSporkActive(SPORK_9_SUPERBLOCKS_ENABLED)) {
        if(CSuperblockManager::IsSuperblockTriggered(nBlockHeight)) {
            if(CSuperblockManager::IsValid(block.vtx[0], nBlockHeight, blockReward)) {
                LogPrint("gobject", "IsBlockValueValid -- Valid superblock at height %d: %s", nBlockHeight, block.vtx[0].ToString());
                // all checks are done in CSuperblock::IsValid, nothing to do here
                return true;
            }

            // triggered but invalid? that's weird
            LogPrintf("IsBlockValueValid -- ERROR: Invalid superblock detected at height %d: %s", nBlockHeight, block.vtx[0].ToString());
            // should NOT allow invalid superblocks, when superblocks are enabled
            strErrorRet = strprintf("invalid superblock detected at height %d", nBlockHeight);
            return false;
        }
        LogPrint("gobject", "IsBlockValueValid -- No triggered superblock detected at height %d\n", nBlockHeight);
        if(!isBlockRewardValueMet) {
            strErrorRet = strprintf("coinbase pays too much at height %d (actual=%d vs limit=%d), exceeded block reward, no triggered superblock detected",
                                    nBlockHeight, block.vtx[0].GetValueOut(), blockReward);
        }
    } else {
        // should NOT allow superblocks at all, when superblocks are disabled
        LogPrint("gobject", "IsBlockValueValid -- Superblocks are disabled, no superblocks allowed\n");
        if(!isBlockRewardValueMet) {
            strErrorRet = strprintf("coinbase pays too much at height %d (actual=%d vs limit=%d), exceeded block reward, superblocks are disabled",
                                    nBlockHeight, block.vtx[0].GetValueOut(), blockReward);
        }
    }

    // it MUST be a regular block
    return isBlockRewardValueMet;
}

bool IsBlockPayeeValid(const CTransaction& txNew, int nBlockHeight, CAmount blockReward)
{
    if(!councilorSync.IsSynced()) {
        //there is no budget data to use to check anything, let's just accept the longest chain
        if(fDebug) LogPrintf("IsBlockPayeeValid -- WARNING: Client not synced, skipping block payee checks\n");
        return true;
    }

    // we are still using budgets, but we have no data about them anymore,
    // we can only check councilor payments

    const Consensus::Params& consensusParams = Params().GetConsensus();

    if(nBlockHeight < consensusParams.nSuperblockStartBlock) {
        if(mnpayments.IsTransactionValid(txNew, nBlockHeight)) {
            LogPrint("mnpayments", "IsBlockPayeeValid -- Valid councilor payment at height %d: %s", nBlockHeight, txNew.ToString());
            return true;
        }

        int nOffset = nBlockHeight % consensusParams.nBudgetPaymentsCycleBlocks;
        if(nBlockHeight >= consensusParams.nBudgetPaymentsStartBlock &&
            nOffset < consensusParams.nBudgetPaymentsWindowBlocks) {
            if(!sporkManager.IsSporkActive(SPORK_13_OLD_SUPERBLOCK_FLAG)) {
                // no budget blocks should be accepted here, if SPORK_13_OLD_SUPERBLOCK_FLAG is disabled
                LogPrint("gobject", "IsBlockPayeeValid -- ERROR: Client synced but budget spork is disabled and councilor payment is invalid\n");
                return false;
            }
            // NOTE: this should never happen in real, SPORK_13_OLD_SUPERBLOCK_FLAG MUST be disabled when 12.1 starts to go live
            LogPrint("gobject", "IsBlockPayeeValid -- WARNING: Probably valid budget block, have no data, accepting\n");
            // TODO: reprocess blocks to make sure they are legit?
            return true;
        }

        if(sporkManager.IsSporkActive(SPORK_8_COUNCILOR_PAYMENT_ENFORCEMENT)) {
            LogPrintf("IsBlockPayeeValid -- ERROR: Invalid councilor payment detected at height %d: %s", nBlockHeight, txNew.ToString());
            return false;
        }

        LogPrintf("IsBlockPayeeValid -- WARNING: Councilor payment enforcement is disabled, accepting any payee\n");
        return true;
    }

    // superblocks started
    // SEE IF THIS IS A VALID SUPERBLOCK

    if(sporkManager.IsSporkActive(SPORK_9_SUPERBLOCKS_ENABLED)) {
        if(CSuperblockManager::IsSuperblockTriggered(nBlockHeight)) {
            if(CSuperblockManager::IsValid(txNew, nBlockHeight, blockReward)) {
                LogPrint("gobject", "IsBlockPayeeValid -- Valid superblock at height %d: %s", nBlockHeight, txNew.ToString());
                return true;
            }

            LogPrintf("IsBlockPayeeValid -- ERROR: Invalid superblock detected at height %d: %s", nBlockHeight, txNew.ToString());
            // should NOT allow such superblocks, when superblocks are enabled
            return false;
        }
        // continue validation, should pay MN
        LogPrint("gobject", "IsBlockPayeeValid -- No triggered superblock detected at height %d\n", nBlockHeight);
    } else {
        // should NOT allow superblocks at all, when superblocks are disabled
        LogPrint("gobject", "IsBlockPayeeValid -- Superblocks are disabled, no superblocks allowed\n");
    }

    // IF THIS ISN'T A SUPERBLOCK OR SUPERBLOCK IS INVALID, IT SHOULD PAY A COUNCILOR DIRECTLY
    if(mnpayments.IsTransactionValid(txNew, nBlockHeight)) {
        LogPrint("mnpayments", "IsBlockPayeeValid -- Valid councilor payment at height %d: %s", nBlockHeight, txNew.ToString());
        return true;
    }

    if(sporkManager.IsSporkActive(SPORK_8_COUNCILOR_PAYMENT_ENFORCEMENT)) {
        LogPrintf("IsBlockPayeeValid -- ERROR: Invalid councilor payment detected at height %d: %s", nBlockHeight, txNew.ToString());
        return false;
    }

    LogPrintf("IsBlockPayeeValid -- WARNING: Councilor payment enforcement is disabled, accepting any payee\n");
    return true;
}

void FillBlockPayments(CMutableTransaction& txNew, int nBlockHeight, CAmount blockReward, CTxOut& txoutCouncilorRet, std::vector<CTxOut>& voutSuperblockRet)
{
    // only create superblocks if spork is enabled AND if superblock is actually triggered
    // (height should be validated inside)
    if(sporkManager.IsSporkActive(SPORK_9_SUPERBLOCKS_ENABLED) &&
        CSuperblockManager::IsSuperblockTriggered(nBlockHeight)) {
            LogPrint("gobject", "FillBlockPayments -- triggered superblock creation at height %d\n", nBlockHeight);
            CSuperblockManager::CreateSuperblock(txNew, nBlockHeight, voutSuperblockRet);
            return;
    }

    // FILL BLOCK PAYEE WITH COUNCILOR PAYMENT OTHERWISE
    mnpayments.FillBlockPayee(txNew, nBlockHeight, blockReward, txoutCouncilorRet);
    LogPrint("mnpayments", "FillBlockPayments -- nBlockHeight %d blockReward %lld txoutCouncilorRet %s txNew %s",
                            nBlockHeight, blockReward, txoutCouncilorRet.ToString(), txNew.ToString());
}

std::string GetRequiredPaymentsString(int nBlockHeight)
{
    // IF WE HAVE A ACTIVATED TRIGGER FOR THIS HEIGHT - IT IS A SUPERBLOCK, GET THE REQUIRED PAYEES
    if(CSuperblockManager::IsSuperblockTriggered(nBlockHeight)) {
        return CSuperblockManager::GetRequiredPaymentsString(nBlockHeight);
    }

    // OTHERWISE, PAY COUNCILOR
    return mnpayments.GetRequiredPaymentsString(nBlockHeight);
}

void CCouncilorPayments::Clear()
{
    LOCK2(cs_mapCouncilorBlocks, cs_mapCouncilorPaymentVotes);
    mapCouncilorBlocks.clear();
    mapCouncilorPaymentVotes.clear();
}

bool CCouncilorPayments::CanVote(COutPoint outCouncilor, int nBlockHeight)
{
    LOCK(cs_mapCouncilorPaymentVotes);

    if (mapCouncilorsLastVote.count(outCouncilor) && mapCouncilorsLastVote[outCouncilor] == nBlockHeight) {
        return false;
    }

    //record this councilor voted
    mapCouncilorsLastVote[outCouncilor] = nBlockHeight;
    return true;
}

/**
*   FillBlockPayee
*
*   Fill Councilor ONLY payment block
*/

void CCouncilorPayments::FillBlockPayee(CMutableTransaction& txNew, int nBlockHeight, CAmount blockReward, CTxOut& txoutCouncilorRet)
{
    // make sure it's not filled yet
    txoutCouncilorRet = CTxOut();

    CScript payee;

    if(!mnpayments.GetBlockPayee(nBlockHeight, payee)) {
        // no councilor detected...
        int nCount = 0;
        councilor_info_t mnInfo;
        if(!mnodeman.GetNextCouncilorInQueueForPayment(nBlockHeight, true, nCount, mnInfo)) {
            // ...and we can't calculate it on our own
            LogPrintf("CCouncilorPayments::FillBlockPayee -- Failed to detect councilor to pay\n");
            return;
        }
        // fill payee with locally calculated winner and hope for the best
        payee = GetScriptForDestination(mnInfo.pubKeyCollateralAddress.GetID());
    }

    // GET COUNCILOR PAYMENT VARIABLES SETUP
    CAmount councilorPayment = GetCouncilorPayment(nBlockHeight, blockReward);

    // split reward between miner ...
    txNew.vout[0].nValue -= councilorPayment;
    // ... and councilor
    txoutCouncilorRet = CTxOut(councilorPayment, payee);
    txNew.vout.push_back(txoutCouncilorRet);

    CTxDestination address1;
    ExtractDestination(payee, address1);
    CBitcoinAddress address2(address1);

    LogPrintf("CCouncilorPayments::FillBlockPayee -- Councilor payment %lld to %s\n", councilorPayment, address2.ToString());
}

int CCouncilorPayments::GetMinCouncilorPaymentsProto() {
    return sporkManager.IsSporkActive(SPORK_10_COUNCILOR_PAY_UPDATED_NODES)
            ? MIN_COUNCILOR_PAYMENT_PROTO_VERSION_2
            : MIN_COUNCILOR_PAYMENT_PROTO_VERSION_1;
}

void CCouncilorPayments::ProcessMessage(CNode* pfrom, std::string& strCommand, CDataStream& vRecv, CConnman& connman)
{
    // Ignore any payments messages until councilor list is synced
    if(!councilorSync.IsCouncilorListSynced()) return;

    if(fLiteMode) return; // disable all Schmeckles specific functionality

    if (strCommand == NetMsgType::COUNCILORPAYMENTSYNC) { //Councilor Payments Request Sync

        // Ignore such requests until we are fully synced.
        // We could start processing this after councilor list is synced
        // but this is a heavy one so it's better to finish sync first.
        if (!councilorSync.IsSynced()) return;

        int nCountNeeded;
        vRecv >> nCountNeeded;

        if(netfulfilledman.HasFulfilledRequest(pfrom->addr, NetMsgType::COUNCILORPAYMENTSYNC)) {
            // Asking for the payments list multiple times in a short period of time is no good
            LogPrintf("COUNCILORPAYMENTSYNC -- peer already asked me for the list, peer=%d\n", pfrom->id);
            Misbehaving(pfrom->GetId(), 20);
            return;
        }
        netfulfilledman.AddFulfilledRequest(pfrom->addr, NetMsgType::COUNCILORPAYMENTSYNC);

        Sync(pfrom, connman);
        LogPrintf("COUNCILORPAYMENTSYNC -- Sent Councilor payment votes to peer %d\n", pfrom->id);

    } else if (strCommand == NetMsgType::COUNCILORPAYMENTVOTE) { // Councilor Payments Vote for the Winner

        CCouncilorPaymentVote vote;
        vRecv >> vote;

        if(pfrom->nVersion < GetMinCouncilorPaymentsProto()) return;

        uint256 nHash = vote.GetHash();

        pfrom->setAskFor.erase(nHash);

        {
            LOCK(cs_mapCouncilorPaymentVotes);
            if(mapCouncilorPaymentVotes.count(nHash)) {
                LogPrint("mnpayments", "COUNCILORPAYMENTVOTE -- hash=%s, nHeight=%d seen\n", nHash.ToString(), nCachedBlockHeight);
                return;
            }

            // Avoid processing same vote multiple times
            mapCouncilorPaymentVotes[nHash] = vote;
            // but first mark vote as non-verified,
            // AddPaymentVote() below should take care of it if vote is actually ok
            mapCouncilorPaymentVotes[nHash].MarkAsNotVerified();
        }

        int nFirstBlock = nCachedBlockHeight - GetStorageLimit();
        if(vote.nBlockHeight < nFirstBlock || vote.nBlockHeight > nCachedBlockHeight+20) {
            LogPrint("mnpayments", "COUNCILORPAYMENTVOTE -- vote out of range: nFirstBlock=%d, nBlockHeight=%d, nHeight=%d\n", nFirstBlock, vote.nBlockHeight, nCachedBlockHeight);
            return;
        }

        std::string strError = "";
        if(!vote.IsValid(pfrom, nCachedBlockHeight, strError, connman)) {
            LogPrint("mnpayments", "COUNCILORPAYMENTVOTE -- invalid message, error: %s\n", strError);
            return;
        }

        if(!CanVote(vote.vinCouncilor.prevout, vote.nBlockHeight)) {
            LogPrintf("COUNCILORPAYMENTVOTE -- councilor already voted, councilor=%s\n", vote.vinCouncilor.prevout.ToStringShort());
            return;
        }

        councilor_info_t mnInfo;
        if(!mnodeman.GetCouncilorInfo(vote.vinCouncilor.prevout, mnInfo)) {
            // mn was not found, so we can't check vote, some info is probably missing
            LogPrintf("COUNCILORPAYMENTVOTE -- councilor is missing %s\n", vote.vinCouncilor.prevout.ToStringShort());
            mnodeman.AskForMN(pfrom, vote.vinCouncilor.prevout, connman);
            return;
        }

        int nDos = 0;
        if(!vote.CheckSignature(mnInfo.pubKeyCouncilor, nCachedBlockHeight, nDos)) {
            if(nDos) {
                LogPrintf("COUNCILORPAYMENTVOTE -- ERROR: invalid signature\n");
                Misbehaving(pfrom->GetId(), nDos);
            } else {
                // only warn about anything non-critical (i.e. nDos == 0) in debug mode
                LogPrint("mnpayments", "COUNCILORPAYMENTVOTE -- WARNING: invalid signature\n");
            }
            // Either our info or vote info could be outdated.
            // In case our info is outdated, ask for an update,
            mnodeman.AskForMN(pfrom, vote.vinCouncilor.prevout, connman);
            // but there is nothing we can do if vote info itself is outdated
            // (i.e. it was signed by a mn which changed its key),
            // so just quit here.
            return;
        }

        CTxDestination address1;
        ExtractDestination(vote.payee, address1);
        CBitcoinAddress address2(address1);

        LogPrint("mnpayments", "COUNCILORPAYMENTVOTE -- vote: address=%s, nBlockHeight=%d, nHeight=%d, prevout=%s, hash=%s new\n",
                    address2.ToString(), vote.nBlockHeight, nCachedBlockHeight, vote.vinCouncilor.prevout.ToStringShort(), nHash.ToString());

        if(AddPaymentVote(vote)){
            vote.Relay(connman);
            councilorSync.BumpAssetLastTime("COUNCILORPAYMENTVOTE");
        }
    }
}

bool CCouncilorPaymentVote::Sign()
{
    std::string strError;
    std::string strMessage = vinCouncilor.prevout.ToStringShort() +
                boost::lexical_cast<std::string>(nBlockHeight) +
                ScriptToAsmStr(payee);

    if(!CMessageSigner::SignMessage(strMessage, vchSig, activeCouncilor.keyCouncilor)) {
        LogPrintf("CCouncilorPaymentVote::Sign -- SignMessage() failed\n");
        return false;
    }

    if(!CMessageSigner::VerifyMessage(activeCouncilor.pubKeyCouncilor, vchSig, strMessage, strError)) {
        LogPrintf("CCouncilorPaymentVote::Sign -- VerifyMessage() failed, error: %s\n", strError);
        return false;
    }

    return true;
}

bool CCouncilorPayments::GetBlockPayee(int nBlockHeight, CScript& payee)
{
    if(mapCouncilorBlocks.count(nBlockHeight)){
        return mapCouncilorBlocks[nBlockHeight].GetBestPayee(payee);
    }

    return false;
}

// Is this councilor scheduled to get paid soon?
// -- Only look ahead up to 8 blocks to allow for propagation of the latest 2 blocks of votes
bool CCouncilorPayments::IsScheduled(CCouncilor& mn, int nNotBlockHeight)
{
    LOCK(cs_mapCouncilorBlocks);

    if(!councilorSync.IsCouncilorListSynced()) return false;

    CScript mnpayee;
    mnpayee = GetScriptForDestination(mn.pubKeyCollateralAddress.GetID());

    CScript payee;
    for(int64_t h = nCachedBlockHeight; h <= nCachedBlockHeight + 8; h++){
        if(h == nNotBlockHeight) continue;
        if(mapCouncilorBlocks.count(h) && mapCouncilorBlocks[h].GetBestPayee(payee) && mnpayee == payee) {
            return true;
        }
    }

    return false;
}

bool CCouncilorPayments::AddPaymentVote(const CCouncilorPaymentVote& vote)
{
    uint256 blockHash = uint256();
    if(!GetBlockHash(blockHash, vote.nBlockHeight - 101)) return false;

    if(HasVerifiedPaymentVote(vote.GetHash())) return false;

    LOCK2(cs_mapCouncilorBlocks, cs_mapCouncilorPaymentVotes);

    mapCouncilorPaymentVotes[vote.GetHash()] = vote;

    if(!mapCouncilorBlocks.count(vote.nBlockHeight)) {
       CCouncilorBlockPayees blockPayees(vote.nBlockHeight);
       mapCouncilorBlocks[vote.nBlockHeight] = blockPayees;
    }

    mapCouncilorBlocks[vote.nBlockHeight].AddPayee(vote);

    return true;
}

bool CCouncilorPayments::HasVerifiedPaymentVote(uint256 hashIn)
{
    LOCK(cs_mapCouncilorPaymentVotes);
    std::map<uint256, CCouncilorPaymentVote>::iterator it = mapCouncilorPaymentVotes.find(hashIn);
    return it != mapCouncilorPaymentVotes.end() && it->second.IsVerified();
}

void CCouncilorBlockPayees::AddPayee(const CCouncilorPaymentVote& vote)
{
    LOCK(cs_vecPayees);

    BOOST_FOREACH(CCouncilorPayee& payee, vecPayees) {
        if (payee.GetPayee() == vote.payee) {
            payee.AddVoteHash(vote.GetHash());
            return;
        }
    }
    CCouncilorPayee payeeNew(vote.payee, vote.GetHash());
    vecPayees.push_back(payeeNew);
}

bool CCouncilorBlockPayees::GetBestPayee(CScript& payeeRet)
{
    LOCK(cs_vecPayees);

    if(!vecPayees.size()) {
        LogPrint("mnpayments", "CCouncilorBlockPayees::GetBestPayee -- ERROR: couldn't find any payee\n");
        return false;
    }

    int nVotes = -1;
    BOOST_FOREACH(CCouncilorPayee& payee, vecPayees) {
        if (payee.GetVoteCount() > nVotes) {
            payeeRet = payee.GetPayee();
            nVotes = payee.GetVoteCount();
        }
    }

    return (nVotes > -1);
}

bool CCouncilorBlockPayees::HasPayeeWithVotes(const CScript& payeeIn, int nVotesReq)
{
    LOCK(cs_vecPayees);

    BOOST_FOREACH(CCouncilorPayee& payee, vecPayees) {
        if (payee.GetVoteCount() >= nVotesReq && payee.GetPayee() == payeeIn) {
            return true;
        }
    }

    LogPrint("mnpayments", "CCouncilorBlockPayees::HasPayeeWithVotes -- ERROR: couldn't find any payee with %d+ votes\n", nVotesReq);
    return false;
}

bool CCouncilorBlockPayees::IsTransactionValid(const CTransaction& txNew)
{
    LOCK(cs_vecPayees);

    int nMaxSignatures = 0;
    std::string strPayeesPossible = "";

    CAmount nCouncilorPayment = GetCouncilorPayment(nBlockHeight, txNew.GetValueOut());

    //require at least MNPAYMENTS_SIGNATURES_REQUIRED signatures

    BOOST_FOREACH(CCouncilorPayee& payee, vecPayees) {
        if (payee.GetVoteCount() >= nMaxSignatures) {
            nMaxSignatures = payee.GetVoteCount();
        }
    }

    // if we don't have at least MNPAYMENTS_SIGNATURES_REQUIRED signatures on a payee, approve whichever is the longest chain
    if(nMaxSignatures < MNPAYMENTS_SIGNATURES_REQUIRED) return true;

    BOOST_FOREACH(CCouncilorPayee& payee, vecPayees) {
        if (payee.GetVoteCount() >= MNPAYMENTS_SIGNATURES_REQUIRED) {
            BOOST_FOREACH(CTxOut txout, txNew.vout) {
                if (payee.GetPayee() == txout.scriptPubKey && nCouncilorPayment == txout.nValue) {
                    LogPrint("mnpayments", "CCouncilorBlockPayees::IsTransactionValid -- Found required payment\n");
                    return true;
                }
            }

            CTxDestination address1;
            ExtractDestination(payee.GetPayee(), address1);
            CBitcoinAddress address2(address1);

            if(strPayeesPossible == "") {
                strPayeesPossible = address2.ToString();
            } else {
                strPayeesPossible += "," + address2.ToString();
            }
        }
    }

    LogPrintf("CCouncilorBlockPayees::IsTransactionValid -- ERROR: Missing required payment, possible payees: '%s', amount: %f SCHMECKLES\n", strPayeesPossible, (float)nCouncilorPayment/COIN);
    return false;
}

std::string CCouncilorBlockPayees::GetRequiredPaymentsString()
{
    LOCK(cs_vecPayees);

    std::string strRequiredPayments = "Unknown";

    BOOST_FOREACH(CCouncilorPayee& payee, vecPayees)
    {
        CTxDestination address1;
        ExtractDestination(payee.GetPayee(), address1);
        CBitcoinAddress address2(address1);

        if (strRequiredPayments != "Unknown") {
            strRequiredPayments += ", " + address2.ToString() + ":" + boost::lexical_cast<std::string>(payee.GetVoteCount());
        } else {
            strRequiredPayments = address2.ToString() + ":" + boost::lexical_cast<std::string>(payee.GetVoteCount());
        }
    }

    return strRequiredPayments;
}

std::string CCouncilorPayments::GetRequiredPaymentsString(int nBlockHeight)
{
    LOCK(cs_mapCouncilorBlocks);

    if(mapCouncilorBlocks.count(nBlockHeight)){
        return mapCouncilorBlocks[nBlockHeight].GetRequiredPaymentsString();
    }

    return "Unknown";
}

bool CCouncilorPayments::IsTransactionValid(const CTransaction& txNew, int nBlockHeight)
{
    LOCK(cs_mapCouncilorBlocks);

    if(mapCouncilorBlocks.count(nBlockHeight)){
        return mapCouncilorBlocks[nBlockHeight].IsTransactionValid(txNew);
    }

    return true;
}

void CCouncilorPayments::CheckAndRemove()
{
    if(!councilorSync.IsBlockchainSynced()) return;

    LOCK2(cs_mapCouncilorBlocks, cs_mapCouncilorPaymentVotes);

    int nLimit = GetStorageLimit();

    std::map<uint256, CCouncilorPaymentVote>::iterator it = mapCouncilorPaymentVotes.begin();
    while(it != mapCouncilorPaymentVotes.end()) {
        CCouncilorPaymentVote vote = (*it).second;

        if(nCachedBlockHeight - vote.nBlockHeight > nLimit) {
            LogPrint("mnpayments", "CCouncilorPayments::CheckAndRemove -- Removing old Councilor payment: nBlockHeight=%d\n", vote.nBlockHeight);
            mapCouncilorPaymentVotes.erase(it++);
            mapCouncilorBlocks.erase(vote.nBlockHeight);
        } else {
            ++it;
        }
    }
    LogPrintf("CCouncilorPayments::CheckAndRemove -- %s\n", ToString());
}

bool CCouncilorPaymentVote::IsValid(CNode* pnode, int nValidationHeight, std::string& strError, CConnman& connman)
{
    councilor_info_t mnInfo;

    if(!mnodeman.GetCouncilorInfo(vinCouncilor.prevout, mnInfo)) {
        strError = strprintf("Unknown Councilor: prevout=%s", vinCouncilor.prevout.ToStringShort());
        // Only ask if we are already synced and still have no idea about that Councilor
        if(councilorSync.IsCouncilorListSynced()) {
            mnodeman.AskForMN(pnode, vinCouncilor.prevout, connman);
        }

        return false;
    }

    int nMinRequiredProtocol;
    if(nBlockHeight >= nValidationHeight) {
        // new votes must comply SPORK_10_COUNCILOR_PAY_UPDATED_NODES rules
        nMinRequiredProtocol = mnpayments.GetMinCouncilorPaymentsProto();
    } else {
        // allow non-updated councilors for old blocks
        nMinRequiredProtocol = MIN_COUNCILOR_PAYMENT_PROTO_VERSION_1;
    }

    if(mnInfo.nProtocolVersion < nMinRequiredProtocol) {
        strError = strprintf("Councilor protocol is too old: nProtocolVersion=%d, nMinRequiredProtocol=%d", mnInfo.nProtocolVersion, nMinRequiredProtocol);
        return false;
    }

    // Only councilors should try to check councilor rank for old votes - they need to pick the right winner for future blocks.
    // Regular clients (miners included) need to verify councilor rank for future block votes only.
    if(!fCounciLor && nBlockHeight < nValidationHeight) return true;

    int nRank;

    if(!mnodeman.GetCouncilorRank(vinCouncilor.prevout, nRank, nBlockHeight - 101, nMinRequiredProtocol)) {
        LogPrint("mnpayments", "CCouncilorPaymentVote::IsValid -- Can't calculate rank for councilor %s\n",
                    vinCouncilor.prevout.ToStringShort());
        return false;
    }

    if(nRank > MNPAYMENTS_SIGNATURES_TOTAL) {
        // It's common to have councilors mistakenly think they are in the top 10
        // We don't want to print all of these messages in normal mode, debug mode should print though
        strError = strprintf("Councilor is not in the top %d (%d)", MNPAYMENTS_SIGNATURES_TOTAL, nRank);
        // Only ban for new mnw which is out of bounds, for old mnw MN list itself might be way too much off
        if(nRank > MNPAYMENTS_SIGNATURES_TOTAL*2 && nBlockHeight > nValidationHeight) {
            strError = strprintf("Councilor is not in the top %d (%d)", MNPAYMENTS_SIGNATURES_TOTAL*2, nRank);
            LogPrintf("CCouncilorPaymentVote::IsValid -- Error: %s\n", strError);
            // do not ban nodes before DIP0001 is locked in to avoid banning majority of (old) councilors
            if (fDIP0001WasLockedIn) {
                Misbehaving(pnode->GetId(), 20);
            }
        }
        // Still invalid however
        return false;
    }

    return true;
}

bool CCouncilorPayments::ProcessBlock(int nBlockHeight, CConnman& connman)
{
    // DETERMINE IF WE SHOULD BE VOTING FOR THE NEXT PAYEE

    if(fLiteMode || !fCounciLor) return false;

    // We have little chances to pick the right winner if winners list is out of sync
    // but we have no choice, so we'll try. However it doesn't make sense to even try to do so
    // if we have not enough data about councilors.
    if(!councilorSync.IsCouncilorListSynced()) return false;

    int nRank;

    if (!mnodeman.GetCouncilorRank(activeCouncilor.outpoint, nRank, nBlockHeight - 101, GetMinCouncilorPaymentsProto())) {
        LogPrint("mnpayments", "CCouncilorPayments::ProcessBlock -- Unknown Councilor\n");
        return false;
    }

    if (nRank > MNPAYMENTS_SIGNATURES_TOTAL) {
        LogPrint("mnpayments", "CCouncilorPayments::ProcessBlock -- Councilor not in the top %d (%d)\n", MNPAYMENTS_SIGNATURES_TOTAL, nRank);
        return false;
    }


    // LOCATE THE NEXT COUNCILOR WHICH SHOULD BE PAID

    LogPrintf("CCouncilorPayments::ProcessBlock -- Start: nBlockHeight=%d, councilor=%s\n", nBlockHeight, activeCouncilor.outpoint.ToStringShort());

    // pay to the oldest MN that still had no payment but its input is old enough and it was active long enough
    int nCount = 0;
    councilor_info_t mnInfo;

    if (!mnodeman.GetNextCouncilorInQueueForPayment(nBlockHeight, true, nCount, mnInfo)) {
        LogPrintf("CCouncilorPayments::ProcessBlock -- ERROR: Failed to find councilor to pay\n");
        return false;
    }

    LogPrintf("CCouncilorPayments::ProcessBlock -- Councilor found by GetNextCouncilorInQueueForPayment(): %s\n", mnInfo.vin.prevout.ToStringShort());


    CScript payee = GetScriptForDestination(mnInfo.pubKeyCollateralAddress.GetID());

    CCouncilorPaymentVote voteNew(activeCouncilor.outpoint, nBlockHeight, payee);

    CTxDestination address1;
    ExtractDestination(payee, address1);
    CBitcoinAddress address2(address1);

    LogPrintf("CCouncilorPayments::ProcessBlock -- vote: payee=%s, nBlockHeight=%d\n", address2.ToString(), nBlockHeight);

    // SIGN MESSAGE TO NETWORK WITH OUR COUNCILOR KEYS

    LogPrintf("CCouncilorPayments::ProcessBlock -- Signing vote\n");
    if (voteNew.Sign()) {
        LogPrintf("CCouncilorPayments::ProcessBlock -- AddPaymentVote()\n");

        if (AddPaymentVote(voteNew)) {
            voteNew.Relay(connman);
            return true;
        }
    }

    return false;
}

void CCouncilorPayments::CheckPreviousBlockVotes(int nPrevBlockHeight)
{
    if (!councilorSync.IsWinnersListSynced()) return;

    std::string debugStr;

    debugStr += strprintf("CCouncilorPayments::CheckPreviousBlockVotes -- nPrevBlockHeight=%d, expected voting MNs:\n", nPrevBlockHeight);

    CCouncilorMan::rank_pair_vec_t mns;
    if (!mnodeman.GetCouncilorRanks(mns, nPrevBlockHeight - 101, GetMinCouncilorPaymentsProto())) {
        debugStr += "CCouncilorPayments::CheckPreviousBlockVotes -- GetCouncilorRanks failed\n";
        LogPrint("mnpayments", "%s", debugStr);
        return;
    }

    LOCK2(cs_mapCouncilorBlocks, cs_mapCouncilorPaymentVotes);

    for (int i = 0; i < MNPAYMENTS_SIGNATURES_TOTAL && i < (int)mns.size(); i++) {
        auto mn = mns[i];
        CScript payee;
        bool found = false;

        if (mapCouncilorBlocks.count(nPrevBlockHeight)) {
            for (auto &p : mapCouncilorBlocks[nPrevBlockHeight].vecPayees) {
                for (auto &voteHash : p.GetVoteHashes()) {
                    if (!mapCouncilorPaymentVotes.count(voteHash)) {
                        debugStr += strprintf("CCouncilorPayments::CheckPreviousBlockVotes --   could not find vote %s\n",
                                              voteHash.ToString());
                        continue;
                    }
                    auto vote = mapCouncilorPaymentVotes[voteHash];
                    if (vote.vinCouncilor.prevout == mn.second.vin.prevout) {
                        payee = vote.payee;
                        found = true;
                        break;
                    }
                }
            }
        }

        if (!found) {
            debugStr += strprintf("CCouncilorPayments::CheckPreviousBlockVotes --   %s - no vote received\n",
                                  mn.second.vin.prevout.ToStringShort());
            mapCouncilorsDidNotVote[mn.second.vin.prevout]++;
            continue;
        }

        CTxDestination address1;
        ExtractDestination(payee, address1);
        CBitcoinAddress address2(address1);

        debugStr += strprintf("CCouncilorPayments::CheckPreviousBlockVotes --   %s - voted for %s\n",
                              mn.second.vin.prevout.ToStringShort(), address2.ToString());
    }
    debugStr += "CCouncilorPayments::CheckPreviousBlockVotes -- Councilors which missed a vote in the past:\n";
    for (auto it : mapCouncilorsDidNotVote) {
        debugStr += strprintf("CCouncilorPayments::CheckPreviousBlockVotes --   %s: %d\n", it.first.ToStringShort(), it.second);
    }

    LogPrint("mnpayments", "%s", debugStr);
}

void CCouncilorPaymentVote::Relay(CConnman& connman)
{
    // do not relay until synced
    if (!councilorSync.IsWinnersListSynced()) return;
    CInv inv(MSG_COUNCILOR_PAYMENT_VOTE, GetHash());
    // relay votes only strictly to new nodes until DIP0001 is locked in to avoid being banned by majority of (old) councilors
    connman.RelayInv(inv, fDIP0001WasLockedIn ? mnpayments.GetMinCouncilorPaymentsProto() : MIN_COUNCILOR_PAYMENT_PROTO_VERSION_2);
}

bool CCouncilorPaymentVote::CheckSignature(const CPubKey& pubKeyCouncilor, int nValidationHeight, int &nDos)
{
    // do not ban by default
    nDos = 0;

    std::string strMessage = vinCouncilor.prevout.ToStringShort() +
                boost::lexical_cast<std::string>(nBlockHeight) +
                ScriptToAsmStr(payee);

    std::string strError = "";
    if (!CMessageSigner::VerifyMessage(pubKeyCouncilor, vchSig, strMessage, strError)) {
        // Only ban for future block vote when we are already synced.
        // Otherwise it could be the case when MN which signed this vote is using another key now
        // and we have no idea about the old one.
        if(councilorSync.IsCouncilorListSynced() && nBlockHeight > nValidationHeight) {
            nDos = 20;
        }
        return error("CCouncilorPaymentVote::CheckSignature -- Got bad Councilor payment signature, councilor=%s, error: %s", vinCouncilor.prevout.ToStringShort().c_str(), strError);
    }

    return true;
}

std::string CCouncilorPaymentVote::ToString() const
{
    std::ostringstream info;

    info << vinCouncilor.prevout.ToStringShort() <<
            ", " << nBlockHeight <<
            ", " << ScriptToAsmStr(payee) <<
            ", " << (int)vchSig.size();

    return info.str();
}

// Send only votes for future blocks, node should request every other missing payment block individually
void CCouncilorPayments::Sync(CNode* pnode, CConnman& connman)
{
    LOCK(cs_mapCouncilorBlocks);

    if(!councilorSync.IsWinnersListSynced()) return;

    int nInvCount = 0;

    for(int h = nCachedBlockHeight; h < nCachedBlockHeight + 20; h++) {
        if(mapCouncilorBlocks.count(h)) {
            BOOST_FOREACH(CCouncilorPayee& payee, mapCouncilorBlocks[h].vecPayees) {
                std::vector<uint256> vecVoteHashes = payee.GetVoteHashes();
                BOOST_FOREACH(uint256& hash, vecVoteHashes) {
                    if(!HasVerifiedPaymentVote(hash)) continue;
                    pnode->PushInventory(CInv(MSG_COUNCILOR_PAYMENT_VOTE, hash));
                    nInvCount++;
                }
            }
        }
    }

    LogPrintf("CCouncilorPayments::Sync -- Sent %d votes to peer %d\n", nInvCount, pnode->id);
    connman.PushMessage(pnode, NetMsgType::SYNCSTATUSCOUNT, COUNCILOR_SYNC_MNW, nInvCount);
}

// Request low data/unknown payment blocks in batches directly from some node instead of/after preliminary Sync.
void CCouncilorPayments::RequestLowDataPaymentBlocks(CNode* pnode, CConnman& connman)
{
    if(!councilorSync.IsCouncilorListSynced()) return;

    LOCK2(cs_main, cs_mapCouncilorBlocks);

    std::vector<CInv> vToFetch;
    int nLimit = GetStorageLimit();

    const CBlockIndex *pindex = chainActive.Tip();

    while(nCachedBlockHeight - pindex->nHeight < nLimit) {
        if(!mapCouncilorBlocks.count(pindex->nHeight)) {
            // We have no idea about this block height, let's ask
            vToFetch.push_back(CInv(MSG_COUNCILOR_PAYMENT_BLOCK, pindex->GetBlockHash()));
            // We should not violate GETDATA rules
            if(vToFetch.size() == MAX_INV_SZ) {
                LogPrintf("CCouncilorPayments::SyncLowDataPaymentBlocks -- asking peer %d for %d blocks\n", pnode->id, MAX_INV_SZ);
                connman.PushMessage(pnode, NetMsgType::GETDATA, vToFetch);
                // Start filling new batch
                vToFetch.clear();
            }
        }
        if(!pindex->pprev) break;
        pindex = pindex->pprev;
    }

    std::map<int, CCouncilorBlockPayees>::iterator it = mapCouncilorBlocks.begin();

    while(it != mapCouncilorBlocks.end()) {
        int nTotalVotes = 0;
        bool fFound = false;
        BOOST_FOREACH(CCouncilorPayee& payee, it->second.vecPayees) {
            if(payee.GetVoteCount() >= MNPAYMENTS_SIGNATURES_REQUIRED) {
                fFound = true;
                break;
            }
            nTotalVotes += payee.GetVoteCount();
        }
        // A clear winner (MNPAYMENTS_SIGNATURES_REQUIRED+ votes) was found
        // or no clear winner was found but there are at least avg number of votes
        if(fFound || nTotalVotes >= (MNPAYMENTS_SIGNATURES_TOTAL + MNPAYMENTS_SIGNATURES_REQUIRED)/2) {
            // so just move to the next block
            ++it;
            continue;
        }
        // DEBUG
        DBG (
            // Let's see why this failed
            BOOST_FOREACH(CCouncilorPayee& payee, it->second.vecPayees) {
                CTxDestination address1;
                ExtractDestination(payee.GetPayee(), address1);
                CBitcoinAddress address2(address1);
                printf("payee %s votes %d\n", address2.ToString().c_str(), payee.GetVoteCount());
            }
            printf("block %d votes total %d\n", it->first, nTotalVotes);
        )
        // END DEBUG
        // Low data block found, let's try to sync it
        uint256 hash;
        if(GetBlockHash(hash, it->first)) {
            vToFetch.push_back(CInv(MSG_COUNCILOR_PAYMENT_BLOCK, hash));
        }
        // We should not violate GETDATA rules
        if(vToFetch.size() == MAX_INV_SZ) {
            LogPrintf("CCouncilorPayments::SyncLowDataPaymentBlocks -- asking peer %d for %d payment blocks\n", pnode->id, MAX_INV_SZ);
            connman.PushMessage(pnode, NetMsgType::GETDATA, vToFetch);
            // Start filling new batch
            vToFetch.clear();
        }
        ++it;
    }
    // Ask for the rest of it
    if(!vToFetch.empty()) {
        LogPrintf("CCouncilorPayments::SyncLowDataPaymentBlocks -- asking peer %d for %d payment blocks\n", pnode->id, vToFetch.size());
        connman.PushMessage(pnode, NetMsgType::GETDATA, vToFetch);
    }
}

std::string CCouncilorPayments::ToString() const
{
    std::ostringstream info;

    info << "Votes: " << (int)mapCouncilorPaymentVotes.size() <<
            ", Blocks: " << (int)mapCouncilorBlocks.size();

    return info.str();
}

bool CCouncilorPayments::IsEnoughData()
{
    float nAverageVotes = (MNPAYMENTS_SIGNATURES_TOTAL + MNPAYMENTS_SIGNATURES_REQUIRED) / 2;
    int nStorageLimit = GetStorageLimit();
    return GetBlockCount() > nStorageLimit && GetVoteCount() > nStorageLimit * nAverageVotes;
}

int CCouncilorPayments::GetStorageLimit()
{
    return std::max(int(mnodeman.size() * nStorageCoeff), nMinBlocksToStore);
}

void CCouncilorPayments::UpdatedBlockTip(const CBlockIndex *pindex, CConnman& connman)
{
    if(!pindex) return;

    nCachedBlockHeight = pindex->nHeight;
    LogPrint("mnpayments", "CCouncilorPayments::UpdatedBlockTip -- nCachedBlockHeight=%d\n", nCachedBlockHeight);

    int nFutureBlock = nCachedBlockHeight + 10;

    CheckPreviousBlockVotes(nFutureBlock - 1);
    ProcessBlock(nFutureBlock, connman);
}
