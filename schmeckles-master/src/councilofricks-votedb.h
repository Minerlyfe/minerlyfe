// Copyright (c) 2014-2017 The Dash Core developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef COUNCILOFRICKS_VOTEDB_H
#define COUNCILOFRICKS_VOTEDB_H

#include <list>
#include <map>

#include "councilofricks-vote.h"
#include "serialize.h"
#include "uint256.h"

/**
 * Represents the collection of votes associated with a given CCouncilofRicksObject
 * Recently received votes are held in memory until a maximum size is reached after
 * which older votes a flushed to a disk file.
 *
 * Note: This is a stub implementation that doesn't limit the number of votes held
 * in memory and doesn't flush to disk.
 */
class CCouncilofRicksObjectVoteFile
{
public: // Types
    typedef std::list<CCouncilofRicksVote> vote_l_t;

    typedef vote_l_t::iterator vote_l_it;

    typedef vote_l_t::const_iterator vote_l_cit;

    typedef std::map<uint256,vote_l_it> vote_m_t;

    typedef vote_m_t::iterator vote_m_it;

    typedef vote_m_t::const_iterator vote_m_cit;

private:
    static const int MAX_MEMORY_VOTES = -1;

    int nMemoryVotes;

    vote_l_t listVotes;

    vote_m_t mapVoteIndex;

public:
    CCouncilofRicksObjectVoteFile();

    CCouncilofRicksObjectVoteFile(const CCouncilofRicksObjectVoteFile& other);

    /**
     * Add a vote to the file
     */
    void AddVote(const CCouncilofRicksVote& vote);

    /**
     * Return true if the vote with this hash is currently cached in memory
     */
    bool HasVote(const uint256& nHash) const;

    /**
     * Retrieve a vote cached in memory
     */
    bool GetVote(const uint256& nHash, CCouncilofRicksVote& vote) const;

    int GetVoteCount() {
        return nMemoryVotes;
    }

    std::vector<CCouncilofRicksVote> GetVotes() const;

    CCouncilofRicksObjectVoteFile& operator=(const CCouncilofRicksObjectVoteFile& other);

    void RemoveVotesFromCouncilor(const COutPoint& outpointCouncilor);

    ADD_SERIALIZE_METHODS;

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream& s, Operation ser_action, int nType, int nVersion)
    {
        READWRITE(nMemoryVotes);
        READWRITE(listVotes);
        if(ser_action.ForRead()) {
            RebuildIndex();
        }
    }
private:
    void RebuildIndex();

};

#endif
