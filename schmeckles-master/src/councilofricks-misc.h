// Copyright (c) 2014-2017 The Dash Core developers

// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.
#ifndef COUNCILOFRICKS_MISC_H
#define COUNCILOFRICKS_MISC_H

#include "validation.h"
#include "councilofricks.h"
#include "init.h"

using namespace std;

class CCouncilofRicksVote;

/**
*	Triggers and Settings - 12.2
*	-----------------------------------
*
*	This allows the network fine grained control of the p2p side, including but not limited to:
*		- Which blocks are valid
*		- What it costs to do various things on the network
*
*/


// class CCouncilofRicksTrigger
// {
// 	static &T IsBlockBanned(int n)
// 	{

// 	}
// };

// /*

	
// */

// class CCouncilofRicksSettings
// {
// 	template<typename T>
// 	// strName=trigger, strParamater=ban-block ... obj= tigger.ban-block(args)
// 	static &T GetSetting(std::string strName, &T networkDefault)
// 	{
// 		/*
// 			- get setting from councilor network
// 		*/

// 		return networkDefault;
// 	}
// };

#endif
