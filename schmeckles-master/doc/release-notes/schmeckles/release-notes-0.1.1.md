0.1.1 Release notes
====================


Schmeckles Core version 0.1.1 is now available from:

  https://schmecklespay.io/downloads

Please report bugs using the issue tracker at github:

  https://github.com/schmecklespay/schmeckles/issues


How to Upgrade
--------------

If you are running an older version, shut it down. Wait until it has completely
shut down (which might take a few minutes for older versions), then run the
installer (on Windows) or just copy over /Applications/Schmeckles-Qt (on Mac) or
schmecklesd/schmeckles-qt (on Linux).

**This new version uses transaction indexing by default, you will need to reindex 
the blockchain. To do so, start the client with --reindex.**



0.1.1 changelog
----------------


Forked from Dash 0.12.0.