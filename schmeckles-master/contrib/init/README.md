Sample configuration files for:

SystemD: schmecklesd.service
Upstart: schmecklesd.conf
OpenRC:  schmecklesd.openrc
         schmecklesd.openrcconf
CentOS:  schmecklesd.init
OS X:    org.schmeckles.schmecklesd.plist

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
